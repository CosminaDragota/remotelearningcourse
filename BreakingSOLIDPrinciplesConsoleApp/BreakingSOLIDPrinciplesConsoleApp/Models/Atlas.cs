﻿using System;
using System.Collections.Generic;
using BreakingSOLIDPrinciplesConsoleApp.Service;

namespace BreakingSOLIDPrinciplesConsoleApp.Models
{
    public class Atlas : Book, ILibrary
    {
        private List<Map> Maps { get; }


        // Eugen : this method(the constructor) has more than 3 parameters -> this is
        // a code smell
        // Recomandation : instead of using title, author and publishingHouse as parameters
        // use a Book object that you have already created and has these properties

        //done: I used a Book object, but because Book is an abstract class, 
        // I cannot instantiate it when I create an Atlas
        // Eugen : considering that this is an exercise to break SOLID, than it is ok
        public Atlas(Book book, Map map) : base(book.Title, book.Author, book.PublishingHouse)
        {
            Maps = new List<Map>();
            Maps.Add(map);
        }

        public Atlas(string title, string author, string publishingHouse) : base(title, author, publishingHouse)
        {

        }

        public override string ToString()
        {
            return "Atlas " + base.ToString();
        }
        //Eugen
        // The below code is not breaking the LSP - it is correct
        // LSP refers to the situation in which we have a class A and a class B that inherites from A although this
        // B should not inherit from A

        // The classic example for LSP
        // Base class Duck.cs - the animal duck that flies and is alive (class A)
        // Child class ElectronicDuck.cs - the toy that needs batteries to come "alive" and move, make noise etc(class B)
        // ElectronicDuck should not inherit from Duck but from a ElectronicToy.cs
        public override void ReadBook(Book book)
        {
            if (book is Atlas)
            {
                Console.WriteLine($"I am reading : {book.ToString()}");
            }
            else
            {
                throw new Exception($"{book.ToString()} is not an atlas!");
            }
        }

        // Eugen : Remove the methods with "throw new NotImplementedException();" 
        // or provide implementation for them
        //done
        public void BorrowBook(Book book)
        {
            Console.WriteLine($"Information about the book: {book.ToString()}");
        }

        public void ReturnBook(Book book)
        {
            Console.WriteLine($"The book: {book.ToString()} is back in the library");
        }

        public void AddAtlas(Book atlas)
        {
            Console.WriteLine($"Information about the atlas: {atlas.ToString()}");
        }
        /**
         * Breaking ISP  and SRP because this class is forced to 
         * implement methods that are not of its responsability
         * */
        // Eugen : ok - this is breaking Liskov as it is the code smell called Refused Bequest
        public void AddPhonebook(Book phoneBook)
        {
            Console.WriteLine($"Information about the phonebook: {phoneBook.ToString()}");
        }

        public void ChangePhoneNumber(Person person)
        {
            Console.WriteLine($"{person.ToString()} has a new number!");
        }

        public void AddMapToAtlas(Map map, Book book)
        {
            Console.WriteLine($"New map {map.ToString()} added to book {book.ToString()}");
        }
    }
}
