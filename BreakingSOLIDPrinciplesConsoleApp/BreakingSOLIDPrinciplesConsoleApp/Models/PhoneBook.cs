﻿using System;
using System.Collections.Generic;

namespace BreakingSOLIDPrinciplesConsoleApp.Models
{
    public class PhoneBook : Book
    {
        private List<Person> People { get; set; }

        // Eugen : this method(the constructor) has more than 3 parameters -> this is
        // a code smell
        // Recomandation : instead of using title, author and publishingHouse as parameters
        // use a Book object that you have already created and has these properties
        //Cosmina:if I use a Book object I cannot create new Phonebook because Book is abstract class

        public PhoneBook(string title, string author, string publishingHouse, Person person) : base(title, author, publishingHouse)
        {
            People = new List<Person>();
            People.Add(person);
        }

        // Eugen : this method(the constructor) has more than 3 parameters -> this is
        // a code smell
        // Recomandation : instead of using title, author and publishingHouse as parameters
        // use a Book object that you have already created and has these properties
        //Cosmina:if I use a Book object I cannot create new Phonebook because Book is abstract class
        public PhoneBook(string title, string author, string publishingHouse) : base(title, author, publishingHouse)
        {
        }

        public override string ToString()
        {
            return "Phonebook " + base.ToString();
        }

        // Eugen : See comments from Atlas.cs for this
        public override void ReadBook(Book book)
        {
            if(book is PhoneBook)
            {
                Console.WriteLine($"I am reading : {book.ToString()}");
            }
            else
            {
                throw new Exception($"{book.ToString()} is not a phonebook!");
            }
        }

        // The below code is not breaking the LSP - it is correct
        // LSP refers to the situation in which we have a class A and a class B that inherites from A although this
        // B should not inherit from A

        // The classic example for LSP
        // Base class Duck.cs - the animal duck that flies and is alive (class A)
        // Child class ElectronicDuck.cs - the toy that needs batteries to come "alive" and move, make noise etc(class B)
        // ElectronicDuck should not inherit from Duck but from a ElectronicToy.cs       
    }
}
