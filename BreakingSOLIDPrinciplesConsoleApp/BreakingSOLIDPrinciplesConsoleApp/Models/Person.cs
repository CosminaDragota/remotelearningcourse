﻿namespace BreakingSOLIDPrinciplesConsoleApp.Models
{   // Eugen : nice usage in this class of the enums variants
    public enum Occupation { salesAssistant, scientist }
    public class Person
    {
        private string Name { get; set; }
        private string PhoneNumber { get; set; }
        private Occupation Occupation { get; set; }
        public Person(string name, string number, Occupation occupation)
        {
            Name = name;
            PhoneNumber = number;
            Occupation = occupation;
        }

        public override string ToString()
        {
            return string.Format($"[Name: {Name}; PhoneNumber: {PhoneNumber}]");
        }
        /***
         * Breaking OCP because I should change the method if I added more occupations to the enum
         * One right way to implement this would be to make Person abstract and this method BorrowBook 
         * abstract and to create the classes SalesAssistant and Scientist and both to inherit Person
         * this method should be implemented in both classes
         * or the classes SalesAssistant and Scientist should implement the same IPerson interface
         * 
         * Breaking SRP because this method should be declared in an interface like IManagePerson,
         * but Person is a model class, which should only have methods like Hashcode/Equals
         * */
        public bool CanBorrowBook(PhoneBook phoneBook)
        {
            if (Occupation == Occupation.salesAssistant)
            {
                return true;
            }
            else if (Occupation == Occupation.scientist)
            {
                return false;
            }
           return false;
        }
    }
}
