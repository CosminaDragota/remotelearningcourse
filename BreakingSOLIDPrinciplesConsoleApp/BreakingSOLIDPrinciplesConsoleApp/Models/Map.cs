﻿using System;

namespace BreakingSOLIDPrinciplesConsoleApp.Models
{
    public enum Dimension { large, medium, small }
    public enum Colour { polychrome, monochrome }
    public class Map
    {
        private string Name { get; set; }
        private Dimension Dimension { get; set; }
        private Colour Colour { get; set; }

        public Map(string name, Dimension dimension, Colour colour)
        {
            Name = name;
            Dimension = dimension;
            Colour = colour;
        }

        public override string ToString()
        {
            return string.Format($"[Name: {Name}; Dimension: {Dimension}; Colour: {Colour}]");
        }

        /**
         * Breaking SRP because Map cannot be bought by itself
         * It also is a model class and it should only have other methods like Hashcode/Equals
         * */
         // Eugen - ok, this is firstyy breaking the correct abstraction - the "A" from "APIE"
        public void BuyMap(Map map)
        {
            Console.WriteLine($"I bought the map {map.Name}");
        }

        // Eugen : good illustration OCP break
        /***
         * Breaking OCP because I should change the method if I added more colours to the enum
         * and one correct way would be to create the classes MonochromeMap and PolychromeMap 
         * that inherit from abstract class Map or 
         * MonochromeMap and PolychromeMap should implements the same IMap interface
         
         * Breaking SRP because this method should be declared in an interface like IManageMap,
         * but Person is a model class, which should only have methods like Hashcode/Equals
         * */
        public void ShowColour(Map map)
        {
            if (map.Colour == Colour.monochrome)
            {
                Console.WriteLine("I am a monochrome map!");
            }

            else if (map.Colour == Colour.polychrome)
            {
                Console.WriteLine("I am a polychrome map!");
            }
        }
    }
}
