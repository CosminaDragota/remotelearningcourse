﻿using System;

namespace BreakingSOLIDPrinciplesConsoleApp.Models
{
    public abstract class Book
    {
        public string Title {get;set;}
        public string Author { get; set; }
        public string PublishingHouse { get; set; }

        // Eugen : because the class Book is abstract, the constructor should be protected
        // why ? an abstract class is never instantiated and its functionalities will be accessed only 
        // after the class will be implemented/ inherited by another non-abstract class
        protected Book(string title, string author, string publishingHouse)
        {
            Title = title;
            Author = author;
            PublishingHouse = publishingHouse;
        }

        // Eugen : Good : usage of string interpolation
        public override string ToString()
        {
            return string.Format($"Title:{Title};Author:{Author};PublishingHouse:{PublishingHouse}");
        }
        
        // Eugen : good illustration of SRP break
        /**
         * Breaking SRP because Book cannot be read by itself
         * It also is a model class and it should only have other methods like Hashcode/Equals
         **/
        public abstract void ReadBook(Book book);

    }
}
