﻿using BreakingSOLIDPrinciplesConsoleApp.Models;

namespace BreakingSOLIDPrinciplesConsoleApp.Service
{
    public interface ILibrary
    {
        void BorrowBook(Book book);
        void ReturnBook(Book book);
        void AddAtlas(Book atlas);
        void AddPhonebook(Book phoneBook);

        // Eugen: Correct illustration of ISP break
        /**
         * Breaking ISP because the interface has more than one responsibility
         * The method ChangePhoneNumber should be in another interface like IManagePersons
         * The method AddMapToAtlas should be in another interface like IManageMaps
         * */
        void ChangePhoneNumber(Person person);
        void AddMapToAtlas(Map map,Book book);

    }
}
