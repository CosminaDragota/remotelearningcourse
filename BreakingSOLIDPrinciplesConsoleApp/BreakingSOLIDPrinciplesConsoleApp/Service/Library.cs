﻿using System;
using System.Collections.Generic;
using BreakingSOLIDPrinciplesConsoleApp.Models;

namespace BreakingSOLIDPrinciplesConsoleApp.Service
{
    // Eugen : if "the class is forced to implement methods that have nothing 
    // to do with its responsibility"" than SRP is broken not ISP
    //I thought than ISP is SRP applied to the interfaces, this is why I wrote it breaks ISP
    /**Breaking ISP because the class is forced to implement methods that have nothing 
     * to do with its responsibility
     * */
    class Library : ILibrary
    {
        private List<Book> books;

        public Library()
        {
            books = new List<Book>();
        }

        public void AddAtlas(Book atlas)
        {
            books.Add(atlas);
        }

        public void AddMapToAtlas(Map map, Book book)
        {
            Console.WriteLine($"New map {map.ToString()} added to book {book.ToString()}");
        }

        public void AddPhonebook(Book phoneBook)
        {
            books.Add(phoneBook);
        }

        // Eugen : Take a look at the below method : DO you really like the way it is displayed ?
        // 1. it has empty lines that should not be empty : ex. 47, 49, 54
        //    the style of the method is not consistent : line 58,59,60 are ok but lines 53-56 and 47-51 are not following the same style
        //    even 53-56 and 47-51
        //    line 62 - empty ?...Why ?
        // 2. Again and again in your applications : //throw new NotImplementedException();
        //    a comment ( besides the one reflecting trademark, author or version of the application) is against clean code rules
        //    it is not helping you, it is only adding useless lines of code to your app that will make the methods longer and hard to read
        //Cosmina:done
        public void BorrowBook(Book book)
        {
            if (book is Atlas)
            {
                Console.WriteLine($"Information about the atlas: {book.ToString()}");
            }
            else if (book is PhoneBook)
            {
                Console.WriteLine($"Information about the phonebook: {book.ToString()}");
            }
            else
            {
                Console.WriteLine($"Information about the book: {book.ToString()}");
            }
        }

        // Eugen : throw new NotImplementedException(); --> :|, delete this and provide implementasion
        //Cosmina:I left these method unimplemented because I wanted to illustrate breaking ISP
        //done, implemented the methods
        public void ChangePhoneNumber(Person person)
        {
            Console.WriteLine($"{person.ToString()} has a new number!");
        }

        public void ReturnBook(Book book)
        {
            Console.WriteLine($"The book: {book.ToString()} is back in the library");
        }
    }
}
