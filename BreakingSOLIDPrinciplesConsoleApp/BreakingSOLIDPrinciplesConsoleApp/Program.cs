﻿using System;
using BreakingSOLIDPrinciplesConsoleApp.Service;
using BreakingSOLIDPrinciplesConsoleApp.Models;
namespace BreakingSOLIDPrinciplesConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ILibrary library = new Library();
            Book atlas = new Atlas("Geographic Atlas", "Random", "Maxim Bit");
            Book phonebook = new PhoneBook("Phone book", "Primaria Cluj-Napoca", "Coera");
            library.AddAtlas(atlas);
            library.AddPhonebook(phonebook);
            library.BorrowBook(atlas);
            library.BorrowBook(phonebook);
            /**
             * breaks the Liskov Substitution Principle because the validation would fail 
             * if you provide a Phonebook object instead of an Atlas 
             * and vice versa.
             * Throws exception if you decomment the ReadBook calls
             **/
            atlas.ReadBook(atlas);
            phonebook.ReadBook(phonebook);

            // Eugen : good illustration of DIP break
            /**
             * breaks the Dependency Inversion Principle because class Atlass depends 
             * on another class (Map), not an abstraction
             * */
            Map romania = new Map("Romania", Dimension.large, Colour.monochrome);
            Map italy = new Map("Italy", Dimension.small, Colour.polychrome);

            //cannot instantiate Atlas class because the constructor has as first parameter 
            //an object Book which is abstract
            //Book natGeoAtlas = new Atlas("NAT Geo", "Ben Howard", "Coera", romania);
            //Book geoWildAtlas = new Atlas("Geo Wild", "Ben Howard", "Coera", italy);

            /**
             * breaks the Dependency Inversion Principle because class Phonebook depends 
             * on another class (Person), not an abstraction
             * */
            // Eugen : ok
            Person person = new Person("Cosmina", "07********", Occupation.salesAssistant);
            Person person2 = new Person("Maria", "07********", Occupation.scientist);
            PhoneBook clujNumbers = new PhoneBook("Cluj Numbers","Primaria CLuj","Adevarul",person);
            PhoneBook brasovNumbers = new PhoneBook("Brasov Numbers", "Primaria Brasov", "Adevarul", person2);

            Console.ReadKey();
        }
    }
}
