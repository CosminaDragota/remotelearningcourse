﻿using System;

namespace OopPillarsConsoleApp
{
    class Person
    {
        private string FirstName { get; set; }
        public string LastName { get; set; }
        private long Cnp { get; set; }

        public Person() { }

        public Person(string FirstName, string LastName, long Cnp)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Cnp = Cnp;
        }

        public override string ToString()
        {
            return string.Format($"[First Name: {FirstName}; Last Name: {LastName}; CNP: {Cnp}; ]");
        }

        public override int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            result = prime * result + (int)(Cnp ^ (Cnp >> 32));
            result = prime * result + ((FirstName == null) ? 0 : FirstName.GetHashCode());
            result = prime * result + ((LastName == null) ? 0 : LastName.GetHashCode());

            return result;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null || !(obj is Person))
                return false;
            else
                return FirstName == ((Person)obj).FirstName && LastName == ((Person)obj).LastName && Cnp == ((Person)obj).Cnp;

        }


    }
}
