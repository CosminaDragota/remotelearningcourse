﻿using System;
using System.Collections.Generic;

namespace OopPillarsConsoleApp
{
    class Bank : IBank
    {
        private Dictionary<Person, HashSet<Account>> Dictionary { get; set; }

        public Bank() { Dictionary = new Dictionary<Person, HashSet<Account>>(); }

        public void AddAccount(Account a, Person p)
        {
            HashSet<Account> accounts = new HashSet<Account>();
            Random r = new Random();
            Dictionary.TryGetValue(p, out accounts); 
            if(a is SpendingAccount)
            {
                SpendingAccount spend = new SpendingAccount(
                        a.Holder, a.CurrentSum, a.Type);
                accounts.Add(spend);
            }
            else if (a is SavingsAccount)
            {
                SavingsAccount save = new SavingsAccount(
                        a.Holder, a.CurrentSum, a.Type);
                accounts.Add(save);
            }
            else
            {
                Console.WriteLine("Contul nu se poate crea!");
            }

        }

        public void AddClient(Person p)
        {
            
            Random r = new Random();
            HashSet<Account> accounts = new HashSet<Account>();
            if (!Dictionary.ContainsKey(p))
            {
                Dictionary.Add(p, accounts);

                Console.WriteLine("Person added!");
            }
            else
            {
                Console.WriteLine("Persoana este deja client!");
            }

         }          

        public void DeleteAccount(Account a, Person p)
        {
            HashSet<Account> accounts = new HashSet<Account>();
            Dictionary.TryGetValue(p, out accounts);
            //Polymorphism
            foreach (Account acc in accounts)
            {              
                    if (acc.Equals(a))
                    {
                        accounts.Remove(a);
                        Console.WriteLine("Account " + a.ToString() + " removed");
                    }              
            }			
		}

        public void RemoveClient(Person p)
        {
            if (Dictionary.ContainsKey(p))
            {
                Dictionary.Remove(p);
                Console.WriteLine("Person removed!");
            }
            else
            {
                Console.WriteLine("Persoana nu este client al bancii!");
            }

        }

        public void AddMoney(Account a, double s)
        {
            //Polymorphism
            a.DepositSum(s);
            if (a is SavingsAccount) {
                ((SavingsAccount)a).CalcInterest();
            }
        }

        public void SubtractMoney(Account a, double s)
        {
            //Polymorphism
            a.WithdrawSum(s);
            if (a is SavingsAccount) {
                ((SavingsAccount)a).CalcInterest();
            }

        }

        public void ListAccounts(Person holder)
        {
            if (holder != null)
            {
                HashSet<Account> accounts = new HashSet<Account>();
                Dictionary.TryGetValue(holder, out accounts);
                if (accounts.Count!=0)
                {
                    Console.WriteLine("The account list of holder is: ");
                    //Polymorphism
                    foreach (Account a in accounts)
                    {
                        Console.WriteLine(a.ToString());
                    }
                }
                else
                {
                    Console.WriteLine("The client doesn't own an account");
                }

            }

        }
    }
}
