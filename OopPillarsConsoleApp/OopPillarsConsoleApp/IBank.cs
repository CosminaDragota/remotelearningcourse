﻿
namespace OopPillarsConsoleApp
{
    interface IBank
    {
        void AddClient(Person p);
        void RemoveClient(Person p);
        void AddAccount(Account a, Person p);
        void DeleteAccount(Account a, Person p);
        void AddMoney(Account a, double s);
        void SubtractMoney(Account a, double s);
        void ListAccounts(Person p);
        
    }
}
