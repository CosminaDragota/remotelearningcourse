﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OopPillarsConsoleApp
{
     abstract class Account
    {
        public Person Holder { get; set; }
        public double CurrentSum { get; set; }
        public string Type { get; set; }

        public Account() { }

        public Account(Person h, double s, string t)
        {
            this.Holder = h;
            this.CurrentSum =s;
            this.Type = t;
        }

        public virtual void DepositSum(double s) { Console.WriteLine("Deposit sum to account"); }

        public virtual void WithdrawSum(double s) { Console.WriteLine("Withdraw sum from account"); }

        public override string ToString()
        {
            return string.Format("[Holder: {0}; Sum: {1}; Type: {2}; ]", Holder, CurrentSum, Type);
        }

        public override bool Equals(Object obj)
        {
            if (obj == null || !(obj is Account))
                return false;
            else
                return Holder == ((Account)obj).Holder && CurrentSum == ((Account)obj).CurrentSum && Type== ((Account)obj).Type;

        }


    }
}
