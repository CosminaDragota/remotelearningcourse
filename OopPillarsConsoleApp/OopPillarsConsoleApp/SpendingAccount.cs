﻿using System;

namespace OopPillarsConsoleApp
{
    class SpendingAccount : Account
    {
        protected double MaxSum { get; set; }
        protected int NoDeposits{get;set;}
        protected int NoWithdrawals { get; set; }

        public SpendingAccount() { }

        public SpendingAccount(Person h, double s, string t)
        {
            this.Holder = h;
            this.CurrentSum = s;
            this.Type = t;

            this.MaxSum = 500.0;
            this.NoDeposits = 0;
            this.NoWithdrawals = 0;
        }


        public override void DepositSum(double s)
        {
            Console.WriteLine("Deposit sum to SpendingAccount");
            if (s > MaxSum)
            {
                Console.WriteLine("Nu se pot depune sume mai mari decat" + MaxSum);
            }
            else
            {
                this.CurrentSum+= s;
                NoDeposits += 1;
                Console.WriteLine($"O noua depunere in cont!Nr depuneri:{NoDeposits } Suma curenta {this.CurrentSum}");  
            }

        }

        public override void WithdrawSum(double s)
        {
            Console.WriteLine("Withdraw sum from SpendingAccount");

            if (s > MaxSum)
            {
                Console.WriteLine("Nu se pot retrage sume mai mari decat " + MaxSum);
            }
            else if (base.CurrentSum < s)
            {
                Console.WriteLine("Nu se poate retrage aceasta suma! Suma existenta este: " + CurrentSum);
            }
            else if(base.CurrentSum >s)
            {

                this.CurrentSum-= s;
                NoWithdrawals += 1;
                Console.WriteLine($"O noua retragere in cont!Nr retrageri:{ NoWithdrawals} Suma curenta: {CurrentSum}");

            }
        }

        public override string ToString()
        {
            return base.ToString() + string.Format("[Max Sum: {0}; No Depsits: {1}; No Withdrawals: {2}; ]", MaxSum, NoDeposits, NoWithdrawals);
        }

    }
}

