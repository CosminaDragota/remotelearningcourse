﻿using System;

namespace OopPillarsConsoleApp
{
    class SavingsAccount : Account
    {
        protected double Interest { get; set; }
        protected bool Deposit { get; set; }
        protected bool Withdraw { get; set; }
        protected double MinSum { get; set; }

        public SavingsAccount(Person h, double s, string t)
        {
            // The following properties are inherited from Account
            this.Holder = h;
            this.CurrentSum = s;
            this.Type = t;

            this.Interest = 0.03;
            this.Deposit = false;
            this.Withdraw = false;
            this.MinSum = 1500.0;
        }

        public override void DepositSum(double s)
        {
            Console.WriteLine("Deposit sum to SavingsAccount");
            
            if (this.Deposit == false)
            {
                if (s < MinSum)
                {
                    Console.WriteLine("Nu se pot depune sume mai mici decat " + MinSum);
                }
                else
                {
                    this.CurrentSum += s;
                    Deposit = true;
                    Console.WriteLine($"Ati retras suma { s } Noua suma:{ this.CurrentSum}");
                }
            }
            else
            {
                Console.WriteLine("Nu se mai poate efectua nicio depunere in cont! ");
            }
        }

        public override void WithdrawSum(double s)
        {
            Console.WriteLine("Withdraw sum from SavingsAccount");

            if (Withdraw == false)
            {
                if (this.CurrentSum < s)
                {
                    Console.WriteLine("Nu se poate retrage aceasta suma! Suma existenta este: "
                                + this.CurrentSum);
                }
                else
                {
                    this.CurrentSum -= s;
                    Withdraw = true;
                    Console.WriteLine($"Ati retras suma {s } Noua suma:{this.CurrentSum}");
                }
            }
            else
            {
                Console.WriteLine("Nu se mai poate efectua nicio retragere in cont! ");
            }
        }

        public void CalcInterest()
        {
            double interest = Interest * this.CurrentSum;
            this.CurrentSum+= interest;
            Console.WriteLine($"Dobanda: {interest } Suma cu dobanda: {this.CurrentSum}");
        }

        public override string ToString()
        {
            return base.ToString()+ string.Format($"[Interest: {Interest}; Min Sum: {MinSum}; ]" );
        }

    }
}


