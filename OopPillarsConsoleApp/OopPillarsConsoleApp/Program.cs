﻿using System;

namespace OopPillarsConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
         
            Person p = new Person("Cosmina","Dragota", 1234567890123);
            Console.WriteLine(p.ToString());

            //Polymorphism and abstraction

            IBank bank = new Bank();
            bank.AddClient(p);
          
            Account save = new  SavingsAccount(p, 10000.0, "save");
            Account spend = new SpendingAccount(p, 1200.0, "spend");

            bank.AddAccount(save,p);
            bank.AddAccount(spend, p);
            bank.ListAccounts(p);//go to definition to see polymorphism
           
            bank.AddMoney(save, 1900.0);
            bank.SubtractMoney(save, 500.0);
            bank.AddMoney(spend, 600.0);
            bank.SubtractMoney(spend, 1100.0);


            Console.Write("\nPress any key to exit...");
            Console.ReadKey(true);
        }
    }
}
