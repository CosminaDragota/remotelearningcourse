﻿using System.Net;
using System.Net.Sockets;
using ConsoleAppCodeFirst.UI;

namespace ConsoleAdministrator
{
    // Balazs : this class has dual responsibility. One for socket communication, and the other for controller. see S from SOLID
    public class AdminController
    {
        private static ConsoleUI ui = new ConsoleUI();
        private static int port = 8001;
        private static string IpAddress = "127.0.0.1";
        private static Socket ClientSocket = new Socket(AddressFamily
            .InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static IPEndPoint ep = new IPEndPoint(IPAddress.Parse(IpAddress), port);
        private static string menuType = "ADMIN";

        static void Main(string[] args)
        {
            bool repeat = true;
            ClientSocket.Connect(ep);
            ui.ShowMessage("Administator is connected!");

            while (repeat)
            {
                repeat=StartAdminFlow();
                ui.ShowGoodbyeMessage();
            }
        }

        public static bool StartAdminFlow()
        {
            int actionId = 0;
            bool repeat = true;
            do
            {
                actionId = ui.DisplayMenu(menuType);  

                if(actionId == 1 || actionId ==4 || actionId==5 || actionId == 6)
                {
                    ClientSocket.Send(System.Text.Encoding.ASCII.GetBytes(actionId.ToString()), 0, actionId.ToString().Length, SocketFlags.None);

                    ReceiveMesssageFromServer();
                }
                else
                {
                    ui.ShowDefaultOption(menuType);
                }
                repeat = ui.CanDisplayMenu();

            } while (repeat);

            return repeat;
        }

        public static void ReceiveMesssageFromServer()
        {
            byte[] messageFromServer = new byte[5000];
            int size = ClientSocket.Receive(messageFromServer);

            string response = System.Text.Encoding.ASCII.GetString(messageFromServer, 0, size);
            ui.ShowMessage("Server response:\n" + response);
        }
    }
}
