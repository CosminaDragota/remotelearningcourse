﻿using ConsoleAppCodeFirst.DB;
using ConsoleAppCodeFirst.DB.DTO;
using System;
using System.Linq;

namespace ConsoleAppCodeFirst.BL
{
    public class LogTransactionService
    {
        public LogTransactionService()
        {

        }

        public void AddRefillTransaction()
        {
            int beforeAdd, afterAdd = 0;
            try
            {
                using (var context = new DataModel())
                {
                    beforeAdd = context.Transactions.Count();

                    context.Transactions.Add(new Transaction { Description = TransactionDescription.Refill, TransactionDate = DateTime.Today });
                    context.SaveChanges();

                    afterAdd = context.Transactions.Count();

                    if (afterAdd > beforeAdd)
                    {
                        Console.WriteLine("Transaction added successfully!");

                    }
                    else
                    {
                        Console.WriteLine("Could not add transaction!");
                    }
                }
            }catch(Exception e)
            {
                Console.WriteLine($"Exception thrown: {e.Message}");
            }
        }

        public void AddProductTransaction(ProductDTO productDTO)
        {
            int beforeAdd, afterAdd = 0;
            try
            {


                using (var context = new DataModel())
                {
                    beforeAdd = context.Transactions.Count();

                    context.Transactions.Add(new Transaction { Description = TransactionDescription.Product, ProductId = productDTO.Id, TransactionDate = DateTime.Today });
                    context.SaveChanges();

                    afterAdd = context.Transactions.Count();

                    if (afterAdd > beforeAdd)
                    {
                        Console.WriteLine("Transaction added successfully!");

                    }
                    else
                    {
                        Console.WriteLine("Could not add transaction!");
                    }
                }
            }catch(Exception e)
            {
                Console.WriteLine($"Exception thrown: {e.Message}");

            }


        }

    }
}
