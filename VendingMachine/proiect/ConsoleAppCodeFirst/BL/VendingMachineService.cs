﻿using System;
using System.Linq;
using ConsoleAppCodeFirst.DB.DTO;
using ConsoleAppCodeFirst.DB;
using System.Text.RegularExpressions;

namespace ConsoleAppCodeFirst.BL
{
    public class VendingMachineService
    {
        public RefillService refillService = null;
        public ProductService productService=null;
        public PaymentService paymentService = null;
        public VendingMachineService()
        {
            refillService = new RefillService();
            productService = new ProductService();
            paymentService = new PaymentService();
        }

        public void AddCategories()
        {
            refillService.categoryService.AddCategories();
        }

        public void Refill()
        {
            bool selectCategory = false;
            int categoryId = 0;
            do
            {
                refillService.categoryService.ListCategories();
                Console.WriteLine("Select category by ID:");
                try
                {
                    int.TryParse(Console.ReadLine().ToString(), out categoryId);
                    Console.WriteLine($"id: {categoryId}");
                    if (categoryId == 0)
                    {
                        Console.WriteLine("Input not valid! Please enter a number!");
                        selectCategory = true;
                    }
                    else
                    {
                        selectCategory = false;

                    }
                }
                catch (System.NullReferenceException ex)
                {
                    Console.WriteLine($"{ex.Message}! Input not valid!");
                    selectCategory = true;
                }
                if (refillService.CanRefillCategory(categoryId))
                {
                    selectCategory = false;
                    CategoryDTO categoryDTO = refillService.categoryService.GetCategory(categoryId);
                    refillService.Refill(categoryDTO);
                }

                break;
            } while (selectCategory);
        }

        public static void AddCategory()
        {
            using (var context = new DataModel())
            {


                context.Categories.Add(new Category { CategoryName = CategoryName.chocolate });
                context.Categories.Add(new Category { CategoryName = CategoryName.coffee });
                context.Categories.Add(new Category { CategoryName = CategoryName.sticks });
                context.Categories.Add(new Category { CategoryName = CategoryName.soda });



                context.SaveChanges();

                var category = (from c in context.Categories
                                where c.CategoryName == CategoryName.coffee
                                select new CategoryDTO()
                                {
                                    CategoryName = c.CategoryName
                                }).FirstOrDefault();
                foreach (var c in context.Categories)
                {
                    var dto = new CategoryDTO() { CategoryName = c.CategoryName };
                    Console.WriteLine($"{dto.CategoryName}");

                }
            }

            Console.ReadKey();
        }



        public void AddProduct()
        {
            using (var context = new DataModel())
            {
                context.Products.Add(new Product { ProductName = ProductName.Fanta, Price = 7.45, Quantity = 10, CategoryId = 4 });
                context.Products.Add(new Product { ProductName = ProductName.Kinder, Price = 7.45, Quantity = 10, CategoryId = 3 });


                context.SaveChanges();

                var product = (from c in context.Products
                               where c.ProductName == ProductName.Kinder
                               select new ProductDTO()
                               {
                                   ProductName = c.ProductName
                               }).FirstOrDefault();
                foreach (var c in context.Products)
                {
                    var dto = new ProductDTO() { ProductName = c.ProductName };
                    Console.WriteLine($"DTO {dto.ProductName}");

                }

                Console.ReadKey();


            }
        }

        public (double, ProductDTO) SelectProduct()
        {
            int productId, quantity = 0;
            bool selectProduct = false;
            double amount = 0.0;
            ProductDTO productDTO = null;
            productService.ListProducts();
            do
            {
                Console.WriteLine("Insert product ID:");

                try
                {
                    int.TryParse(Console.ReadLine().ToString(), out productId);
                    if (productId == 0)
                    {
                        Console.WriteLine("Input not valid! Please enter a number!");
                        selectProduct = true;
                    }
                    else
                    {
                        selectProduct = false;
                        productDTO = productService.SelectProduct(productId);
                        amount = productDTO.Price;
                        Console.WriteLine($"The amount to pay is: {productDTO.Price}");
                    }
                }
                catch (System.NullReferenceException ex)
                {
                    Console.WriteLine($"{ex.Message}! Input not valid!");
                    selectProduct = true;
                }
            } while (selectProduct);
            return (amount, productDTO);
        }

        
        public bool PayProduct(double amountToPay)
        {
            string MoneyRegex = @"^[a-z]*$";
            double insertedSum, differenceOfMoney = 0.0;
            bool changeAmount = false;
            bool isPaid = false;
            do
            {
                try
                {
                    changeAmount = false;
                    Console.Write("\n Enter amount: ");
                    double.TryParse(Console.ReadLine().ToString(), out insertedSum);

                    if (Regex.IsMatch(insertedSum.ToString(), MoneyRegex))
                    {
                        changeAmount= true;
                        throw new FormatException();

                    }else if(paymentService.IsAmountValid(insertedSum, amountToPay)) 
                    {
                        changeAmount = false;
                        differenceOfMoney=paymentService.GiveChange(insertedSum, amountToPay);
                        Console.WriteLine($"Your change is {differenceOfMoney}");
                        paymentService.UpdateSumOfVendingMachine(amountToPay);
                        isPaid = true;
                    }
                    else
                    {
                        changeAmount = true;
                    }
                }
                catch (FormatException e)
                {
                    Console.WriteLine("Enter a number!");
                }
            } while (changeAmount);

            return isPaid;
        }

        
        




    }
}
