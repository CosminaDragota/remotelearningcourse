﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleAppCodeFirst.DB.DTO;
using ConsoleAppCodeFirst.DB;

namespace ConsoleAppCodeFirst.BL
{
    public class CategoryService
    {
        public CategoryService()
        {

        }

        public bool HasProducts(CategoryDTO category)
        {
            using (var context = new DataModel())
            {
                var products = from p in context.Products
                               where p.CategoryId == category.Id
                               select new ProductDTO()
                               {
                                   Id = p.Id,
                                   ProductName = p.ProductName,
                                   Price = p.Price,
                                   Quantity=p.Quantity,
                                   CategoryId = category.Id
                               };
                foreach (var p in products)
                {
                    Console.WriteLine($"Name: {p.ProductName }, quantity: {p.Quantity}");
                }
                if (products.Count() == 0)
                {
                    Console.WriteLine($"{category.CategoryName} needs refill");
                    return false;
                }
                else
                {
                    Console.WriteLine($"{category.CategoryName} has products");
                    return true;
                }
            }
        }

        public CategoryDTO GetCategory(int id)
        {
            using (var context = new DataModel())
            {
                var category = (from c in context.Categories
                                where c.Id == id
                                select new CategoryDTO()
                                {
                                    Id = c.Id,
                                    CategoryName = c.CategoryName
                                }).FirstOrDefault();
                return category;
            }

        }

        public Action AddCategories = () =>
        {

            using (var context = new DataModel())
            {

                context.Categories.Add(new Category { CategoryName = CategoryName.chocolate });
                context.Categories.Add(new Category { CategoryName = CategoryName.coffee });
                context.Categories.Add(new Category { CategoryName = CategoryName.sticks });
                context.Categories.Add(new Category { CategoryName = CategoryName.soda });

                context.SaveChanges();

                var cat = (from c in context.Categories
                           where c.CategoryName == CategoryName.coffee
                           select new CategoryDTO()
                           {
                               CategoryName = c.CategoryName
                           }).FirstOrDefault();
                foreach (var c in context.Categories)
                {
                    var dto = new CategoryDTO() { CategoryName = c.CategoryName };
                    Console.WriteLine($"{dto.CategoryName}");

                }
            }

        };

        public static Func<IQueryable<CategoryDTO>> GetAll = () =>
        {
            using (var context = new DataModel())
            {

                var categories = from c in context.Categories
                                 select new CategoryDTO
                                 {
                                     Id = c.Id,
                                     CategoryName = c.CategoryName
                                 };
                return categories;
            }
        };

        public Action ListCategories = () =>
          {
              using (var context = new DataModel())
              {

                  List<Category> categories = context.Categories.ToList();
                  foreach (Category c in categories)
                  {
                      Console.WriteLine(c.ToString());

                  }
              }

          };


    }
}
