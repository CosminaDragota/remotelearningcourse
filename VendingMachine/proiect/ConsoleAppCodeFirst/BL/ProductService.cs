﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleAppCodeFirst.DB;
using ConsoleAppCodeFirst.DB.DTO;
using System.Text.RegularExpressions;

namespace ConsoleAppCodeFirst.BL
{
    public class ProductService
    {
        public static IEnumerable<Product> products = null;
        private static readonly Random random = new Random();

        public ProductService()
        {

        }

        private static double RandomNumberBetween(double minValue, double maxValue)
        {
            var next = random.NextDouble();

            return minValue + (next * (maxValue - minValue));
        }


        public Action<CategoryDTO> AddProducts = (CategoryDTO categoryDTO) =>
          {


              try
              {
                  products = GenerateRandomProducts(categoryDTO);
                  using (var context = new DataModel())
                  {
                      context.Products.AddRange(products);
                      context.SaveChanges();

                  }
              }
              catch (Exception e)
              {
                  Console.WriteLine("Eroare addProducts!");
              }


          };

        public static IEnumerable<Product> GenerateRandomProducts(CategoryDTO categoryDTO)
        {
            IEnumerable<Product> productsNew = new List<Product>();
            if (categoryDTO.CategoryName == CategoryName.chocolate)
            {
                try
                {
                    productsNew = productsNew.Concat(new[] {
                    new Product { ProductName = ProductName.Africana, Price=RandomNumberBetween(3.0,5.0), Quantity=10,CategoryId=categoryDTO.Id}
                    ,new Product { ProductName = ProductName.Milka, Price = RandomNumberBetween(3.0, 10.0), Quantity=10,CategoryId = categoryDTO.Id }
                    ,new Product { ProductName = ProductName.Poiana, Price = RandomNumberBetween(3.0, 5.0),Quantity=10, CategoryId = categoryDTO.Id }
                    ,new Product { ProductName = ProductName.Heidi, Price = RandomNumberBetween(3.0, 9.0),Quantity=10, CategoryId = categoryDTO.Id }
                    ,new Product { ProductName = ProductName.Schogetten, Price = RandomNumberBetween(3.0, 4.0),Quantity=10, CategoryId = categoryDTO.Id }
                    ,new Product { ProductName = ProductName.Lindt, Price = RandomNumberBetween(3.0, 12.0),Quantity=10, CategoryId = categoryDTO.Id }

                });
                    int cnt = productsNew.Count();
                    Console.WriteLine("ProducstNew ", cnt);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Eroare GenerateRandomProducts - chocolate!{e.Message}");
                }


            }
            else if (categoryDTO.CategoryName == CategoryName.coffee)
            {

                try
                {
                    productsNew = productsNew.Concat(new[] {
                    new Product { ProductName = ProductName.Tchibo, Price=RandomNumberBetween(30.0,50.0),Quantity=10, CategoryId=categoryDTO.Id}
                    ,new Product { ProductName = ProductName.Lavazza, Price = RandomNumberBetween(30.0, 100.0), Quantity=10,CategoryId = categoryDTO.Id }
                    ,new Product { ProductName = ProductName.Nescafe, Price = RandomNumberBetween(30.0, 6.0), Quantity=10, CategoryId = categoryDTO.Id }
                    ,new Product { ProductName = ProductName.Davidoff, Price = RandomNumberBetween(70.0, 100.0), Quantity=10, CategoryId = categoryDTO.Id }

                });
                    int cnt = productsNew.Count();
                    Console.WriteLine("ProducstNew ", cnt);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Eroare GenerateRandomProducts - coffee!{e.Message}");

                }
            }
            else if (categoryDTO.CategoryName == CategoryName.soda)
            {
                try
                {
                    productsNew = productsNew.Concat(new[] {
                    new Product { ProductName = ProductName.Pepsi, Price=RandomNumberBetween(5.0,10.0),Quantity=10, CategoryId=categoryDTO.Id}
                    ,new Product { ProductName = ProductName.CocaCola, Price = RandomNumberBetween(5.0, 10.0), Quantity=10,CategoryId = categoryDTO.Id }
                    ,new Product { ProductName = ProductName.Fanta, Price = RandomNumberBetween(5.0, 15.0), Quantity=10,CategoryId = categoryDTO.Id }
                    ,new Product { ProductName = ProductName.Santal, Price = RandomNumberBetween(13.0, 19.0), Quantity=10,CategoryId = categoryDTO.Id }
                    ,new Product { ProductName = ProductName.Tymbark, Price = RandomNumberBetween(3.0, 14.0), Quantity=10,CategoryId = categoryDTO.Id }

                });
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Eroare GenerateRandomProducts - soda!{e.Message}");

                }
            }
            else if (categoryDTO.CategoryName == CategoryName.sticks)
            {
                try
                {
                    productsNew = productsNew.Concat(new[] {
                    new Product { ProductName = ProductName.Snickers, Price=RandomNumberBetween(3.0,5.0), Quantity=10, CategoryId=categoryDTO.Id}
                    ,new Product { ProductName = ProductName.Kinder, Price = RandomNumberBetween(3.0, 10.0), Quantity=10, CategoryId = categoryDTO.Id }
                    ,new Product { ProductName = ProductName.Lion, Price = RandomNumberBetween(3.0, 5.0),Quantity=10, CategoryId = categoryDTO.Id }
                    ,new Product { ProductName = ProductName.Twix, Price = RandomNumberBetween(3.0, 9.0), Quantity=10,CategoryId = categoryDTO.Id }
                    ,new Product { ProductName = ProductName.Fitness, Price = RandomNumberBetween(3.0, 4.0), Quantity=10,CategoryId = categoryDTO.Id }

                });

                }
                catch (Exception e)
                {
                    Console.WriteLine($"Eroare GenerateRandomProducts - sticks!{e.Message}");

                }
            }

            else
            {
                Console.WriteLine("Couldn't generate products!");
            }
            return productsNew;
        }

        public List<ProductDTO> ListProducts()
        {
            using (var context = new DataModel())
            {

                List<ProductDTO> productDTOs = GetAll();
                foreach (ProductDTO p in productDTOs)
                {
                    Console.WriteLine(p.ToString());

                }
                return productDTOs;
            }
        }

        public static Func<List<ProductDTO>> GetAll = () =>
        {
            using (var context = new DataModel())
            {

                var products = from p in context.Products
                                 select new ProductDTO
                                 {
                                     Id = p.Id,
                                     ProductName = p.ProductName,
                                     Price=p.Price,
                                     Quantity=p.Quantity,
                                     CategoryId=p.CategoryId
                                 };
                return products.ToList();
            }
        };

        public ProductDTO SelectProduct(int productId)
        {
            List<ProductDTO> productDTOs = GetAll();
            var product = (from p in productDTOs
                           where p.Id == productId
                           select new ProductDTO
                           {
                               Id=p.Id,
                               ProductName=p.ProductName,
                               Price=p.Price,
                               Quantity=p.Quantity,
                               CategoryId=p.CategoryId
                           }).FirstOrDefault();
            Console.WriteLine($"You selected:{product.ToString()}");
            return product;
        }

        public void DecreaseQuantity(ProductDTO productDTO)
        {
            try
            {
                using (var context = new DataModel())
                {
                    
                    (from p in context.Products
                     where p.Id == productDTO.Id
                     select p).ToList().ForEach(x =>x.Quantity=x.Quantity-1);

                    context.SaveChanges();

                    var prod = (from p in context.Products
                                where p.Id == productDTO.Id
                                select p).FirstOrDefault();

                    Console.WriteLine($"Quantity updated successfully to {prod.Quantity}!");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not update quantity!");
            }
        }




    }
}
