﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleAppCodeFirst.DB.DTO;
using ConsoleAppCodeFirst.DB;
namespace ConsoleAppCodeFirst.BL
{
    public class RefillService
    {
        public CategoryService categoryService = null;
        public ProductService productService = null;
        public int existingCategories = 0;
        public Predicate<int> IsCategoryValidPredicate;

        public RefillService()
        {
            categoryService = new CategoryService();
            productService = new ProductService();
            IsCategoryValidPredicate = IsCategoryValid;

        }

        public bool IsCategoryValid(int categoryId)
        {

            if (categoryId < 1)
            {
                Console.WriteLine("Enter a positive number!");
                return false;
            }
            using (var context = new DataModel())
            {
                int firstID = context.Categories.First().Id;
                var last = context.Categories.OrderByDescending(x=>x.Id);
                int lastID = last.First().Id;

                if (categoryId<firstID || categoryId>lastID)
                {
                    Console.WriteLine($"Enter a number between {firstID} and {lastID}!");
                    return false;
                }
                return true;
            }

        }

        public bool CanRefillCategory(int categoryId)
        {
            if (IsCategoryValidPredicate(categoryId))
            {
                CategoryDTO dto = categoryService.GetCategory(categoryId);
                Console.WriteLine($"{dto.CategoryName}");
                bool hasProducts = categoryService.HasProducts(dto);
                if (hasProducts)
                {
                    return false;

                }
                return true;
            }
            else
            {
                Console.WriteLine("Cannot refill category!");
                return false;
            }
        }

        public void Refill(CategoryDTO categoryDTO)
        {
            int beforeAdd, afterAdd = 0;
            try
            {
                using (var context = new DataModel())
                {
                    beforeAdd = context.Products.Count();
                    productService.AddProducts(categoryDTO);
                    afterAdd = context.Products.Count();

                    if (afterAdd > beforeAdd)
                    {
                        Console.WriteLine("Product added successfully!");
                    }
                    else
                    {
                        Console.WriteLine("Could not add product!");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.Message} could not refill!");
            }
        }

        public void RefillQuantities()
        {
            try
            {
                using (var context = new DataModel())
                {
                    (from p in context.Products                    
                     select p).ToList().ForEach(x => x.Quantity = 10);

                    context.SaveChanges();

                    foreach(Product p in context.Products)
                    {
                        if (p.Quantity == 10)
                        {
                            Console.WriteLine($"Quantity of {p.ProductName} updated successfully to {p.Quantity}!");
                        }
                        else
                        {
                            Console.WriteLine($"Quantity of {p.ProductName} unchanged!");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not update quantity!");
            }
        }

        public void RefillSum()
        {
            try
            {
                using (var context = new DataModel())
                {
                    (from p in context.Sums
                     select p).ToList().ForEach(x => x.SumValue = 10000);

                    context.SaveChanges();                   
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not refill sum!");
            }
        }
    }
}
