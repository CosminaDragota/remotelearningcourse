﻿using System;
using System.Linq;
using ConsoleAppCodeFirst.DB;
using System.Collections.Generic;

namespace ConsoleAppCodeFirst.BL
{
    public class ReportService
    {
        public ReportService()
        {

        }

        public void AllSalesReport()
        {
            List<Transaction> transactions = null;
            using (var context = new DataModel())
            {
                var sales = (from t in context.Transactions
                             where t.ProductId != null
                             select t).ToList();
                transactions = sales;

                var distinctProducts = transactions.GroupBy(t => t.ProductId).Select(group => group.First()).ToList();

                foreach (Transaction t in distinctProducts)
                {
                    Console.WriteLine($"Sold {t.Product.ProductName}!");
                }
            }
        }

        public void NumberOfRefillOperations()
        {

            using (var context = new DataModel())
            {
                int numberOfRefills = (from t in context.Transactions
                                       where t.Description == 0
                                       select t).ToList().Count();

                Console.WriteLine($"The vending machine was refilled {numberOfRefills} times!");

            }
        }
    }
}
