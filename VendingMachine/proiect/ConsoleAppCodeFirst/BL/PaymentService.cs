﻿using System;
using ConsoleAppCodeFirst.DB;
using System.Linq;

namespace ConsoleAppCodeFirst.BL
{
    public class PaymentService
    {

        public PaymentService()
        {

        }

        public bool IsAmountValid(double insertedSum, double amountToPay)
        {

            using (var context = new DataModel())
            {
                double sum = context.Sums.Find(1).SumValue;

                if (insertedSum < 0 || insertedSum > sum)
                {
                    Console.WriteLine($"Enter a positive number smaller than {sum}!");
                    return false;
                }

                else if (insertedSum == 0)
                {
                    Console.WriteLine("You must select a product first!");
                    return false;
                }
                else if(insertedSum>=amountToPay)
                {
                    return true;
                }
                else
                {
                    Console.WriteLine("Insert more money!");
                    return false;
                }
            }

        }

        public double GiveChange(double insertedSum, double amountToPay)
        {
            using (var context = new DataModel())
            {
                if (insertedSum > amountToPay)
                {
                    return insertedSum - amountToPay;
                }
                else
                {
                    return 0.0;
                }
            }
        }
        public void UpdateSumOfVendingMachine(double amount)
        {
            try
            {
                using (var context = new DataModel())
                {
                    
                    (from p in context.Sums
                     where p.Id == 1
                     select p).ToList().ForEach(x => x.SumValue = x.SumValue + amount);

                    context.SaveChanges();

                    var prod = (from p in context.Sums
                                where p.Id == 1
                                select p).FirstOrDefault();

                    Console.WriteLine($"Sum updated successfully to {prod.SumValue}!");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not update sum!");
            }
        }
    }
}
