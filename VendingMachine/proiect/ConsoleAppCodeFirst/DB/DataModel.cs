﻿using System.Data.Entity;

namespace ConsoleAppCodeFirst.DB
{
    public class DataModel: DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Sum> Sums { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        public DataModel() : base("name=DataModel")
        {
            Configuration.AutoDetectChangesEnabled = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Category>().HasMany(x => x.Products).WithRequired(x => x.Category).HasForeignKey(x => x.CategoryId);
            modelBuilder.Entity<Transaction>().HasOptional(x => x.Product);
        }
    }
}
