﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppCodeFirst.DB.DTO
{
    public class CategoryDTO
    {
        public int Id { get; set; }

        public CategoryName CategoryName { get; set; }

        public virtual ICollection<Product> Products { get; set; }


        public CategoryDTO()
        {

        }

        public CategoryDTO(int id, CategoryName categoryName)
        {
            Id = id;
            CategoryName = categoryName;
        }

        public override string ToString()
        {
            return string.Format($"{Id}. {CategoryName}");
        }
    }
}
