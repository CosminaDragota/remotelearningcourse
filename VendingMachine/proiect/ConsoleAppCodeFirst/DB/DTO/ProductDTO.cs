﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppCodeFirst.DB.DTO
{
    public class ProductDTO
    {
        public int Id { get; set; }

        public ProductName ProductName { get; set; }

        public double Price { get; set; }

        public int Quantity { get; set; }

        public virtual Category Category { get; set; }
        public int CategoryId { get; set; }

        public ProductDTO()
        {

        }
        public ProductDTO(int id, ProductName productName, double price, int quantity, int categoryId)
        {
            Id = id;
            ProductName = productName;
            Price = price;
            Quantity = quantity;
            CategoryId = categoryId;
        }

        public override string ToString()
        {
            return string.Format($"ID: {Id}\n Name: {ProductName};\n Price: {Price} RON; \n Quantity:{Quantity}\n ");
        }
    }
}
