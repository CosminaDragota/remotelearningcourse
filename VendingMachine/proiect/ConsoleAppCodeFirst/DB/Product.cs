﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
//elimin constructorii si folosesc set
namespace ConsoleAppCodeFirst.DB
{
    public enum ProductName
    {
        Milka, Poiana, Heidi, Africana, Schogetten, Lindt,
        Snickers, Kinder, Lion, Twix, Fitness,
        Pepsi, CocaCola, Tymbark, Fanta, Santal,
        Tchibo, Lavazza, Nescafe, Davidoff
    }

    public class Product
    {
        public const int HashNumber = 13;
        public const int HashCoefficient = 7;
        
        [Key]
        public int Id { get; set; }

        [Column("ProductName")]
        public ProductName ProductName { get; set; }

        [Column("ProductPrice")]
        public double Price { get; set; }

        [Column("Quantity")]
        public int Quantity { get; set; }

        [Column("CategoryID")]
        public virtual Category Category { get; set; }
        public int CategoryId { get; set; }

        public Product()
        {

        }
        public Product(ProductName productName, double price, int quantity, int categoryId)
        {
            ProductName = productName;
            Price = price;
            Quantity = quantity;
            CategoryId = categoryId;
        }

        public override string ToString()
        {
            return string.Format($"Name: {ProductName};\n Price: {Price}; \n Quantity:{Quantity}\n ");
        }


        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Product))
            {
                return false;
            }
            else
            {
                return 
                    ProductName == ((Product)obj).ProductName
                    && Price == ((Product)obj).Price
                    && Quantity == ((Product)obj).Quantity;


            }
        }

        public override int GetHashCode()
        {
            int hash = HashNumber;
            hash = (hash * HashCoefficient) + ProductName.GetHashCode();
            hash = (hash * HashCoefficient) + Price.GetHashCode();
            hash = (hash * HashCoefficient) + Quantity.GetHashCode();


            return hash;
        }
    }
}

