﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
namespace ConsoleAppCodeFirst.DB
{
    public enum TransactionDescription
    {
        Refill,Product
    }

    public class Transaction
    {
        [Key]
        public int Id { get; set; }

        [Column("Description")]
        public TransactionDescription Description { get; set; }

        [Column("ProductID")]
        public virtual Product Product {get;set;}
        public int? ProductId { get; set; }
 
        [Column("TransactionDate")]
        public DateTime? TransactionDate { get; set; }

        public Transaction()
        {

        }

        public Transaction(TransactionDescription description, int productId, DateTime transactionDate)
        {
            Description = description;
            ProductId = productId;
            TransactionDate = transactionDate;
        }

        public override string ToString()
        {
            return string.Format($"Description: {Description}\n ProductId: {ProductId}\n Date: {TransactionDate}\n");
        }
    }
}
