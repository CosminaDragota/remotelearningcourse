﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ConsoleAppCodeFirst.DB
{
    public enum CategoryName
    {
        chocolate, sticks, soda, coffee
    }

    public class Category
    {

        public const int HashNumber = 13;
        public const int HashCoefficient = 7;

        [Key]
        public int Id { get; set; }

        [Column("CategoryName")]
        public CategoryName CategoryName { get; set; }

        public virtual ICollection<Product> Products { get; set; }


        public Category()
        {

        }

        public Category(CategoryName categoryName)
        {
            CategoryName = categoryName;
        }

        public override string ToString()
        {
            return string.Format($"ID: {Id} \n Name: {CategoryName}");
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Category))
            {
                return false;
            }
            else
            {
                return CategoryName == ((Category)obj).CategoryName
                    && Id == ((Category)obj).Id;
            }
        }

        public override int GetHashCode()
        {
            int hash = HashNumber;
            hash = (hash * HashCoefficient) + Id.GetHashCode();
            hash = (hash * HashCoefficient) + CategoryName.GetHashCode();

            return hash;
        }

    }
}
