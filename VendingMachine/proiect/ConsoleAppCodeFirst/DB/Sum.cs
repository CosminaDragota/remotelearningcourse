﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConsoleAppCodeFirst.DB
{
    public class Sum
    {
        [Key]
        public int Id { get; set; }

        [Column("SumValue")]
        public double SumValue { get; set; }

        public Sum()
        {

        }

        public Sum(double sumValue)
        {
            SumValue = sumValue;
        }

        public override string ToString()
        {
            return string.Format($"[ID: {Id}, Value:{SumValue} RON]\n ");
        }
    }
}
