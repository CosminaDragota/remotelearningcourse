﻿using System;
using ConsoleAppCodeFirst.BL;
using System.Text.RegularExpressions;
using ConsoleAppCodeFirst.DB.DTO;

namespace ConsoleAppCodeFirst.UI
{
    public class Program
    {
        private const string AnswerRegex = @"^[a-mo-xz-zA-MO-XZ-Z0-9*#+=-]+$";
        public static VendingMachineService vendingMachineService = new VendingMachineService();
        public static LogTransactionService logTransactionService = new LogTransactionService();
        public static ReportService reportService = new ReportService();

        static void Main(string[] args)
        {
            int actionId = 0;
            double amountToPay = 0.0;
            bool isPaid = false;
            ProductDTO selectedProduct = null;
            do
            {
                Console.WriteLine("Options:\n");
                Console.WriteLine("1.Refill products");
                Console.WriteLine("2.Select product");
                Console.WriteLine("3.Pay Product");
                Console.WriteLine("4.Refill quantities and money");
                Console.WriteLine("5.List products");
                Console.WriteLine("6.All Sales Report");
                Console.WriteLine("7.Number of Refills Report");


                Console.WriteLine("\nEnter option:");
                int.TryParse(Console.ReadLine().ToString(), out actionId);

                switch (actionId)
                {
                    case 1:
                        vendingMachineService.Refill(); break;
                    case 2:
                        (amountToPay,selectedProduct)=vendingMachineService.SelectProduct(); break;
                    case 3:
                        isPaid =vendingMachineService.PayProduct(amountToPay);
                        if (isPaid)
                        {
                            vendingMachineService.productService.DecreaseQuantity(selectedProduct);
                            logTransactionService.AddProductTransaction(selectedProduct);
                        }
                        else
                        {
                            Console.WriteLine($"Product {selectedProduct.ProductName} not paid!");
                        }
                        break;
                    case 4:
                        vendingMachineService.refillService.RefillQuantities();
                        vendingMachineService.refillService.RefillSum();
                        logTransactionService.AddRefillTransaction();
                        break;
                    case 5:
                        vendingMachineService.productService.ListProducts();
                        break;
                    case 6:
                        reportService.AllSalesReport();
                        break;
                    case 7:
                        reportService.NumberOfRefillOperations();
                        break;
                    default:
                        Console.WriteLine("Select an option between 1-7!");
                        break;
                }

            } while (DisplayMenu());

            Console.WriteLine("Have a good day!");
            Console.ReadKey();

        }
        

        public static bool DisplayMenu()
        {
            bool repeat = false;
            do
            {
                try
                {
                    Console.WriteLine("\nDo you want to select another option? [y/n]");
                    string choose = Console.ReadLine().ToString();

                    if (Regex.IsMatch(choose, AnswerRegex))
                    {
                        throw new FormatException();

                    }
                    else
                    {

                        if (choose == "y")
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }


                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Choose a letter: y/n");
                    repeat = true;
                }
            } while (repeat);
            return true;

        }

        
    }
}
