﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleVMDictionary
{
    class ContainableItem:Product
    {
        public (int, int) Position;

        // Eugen : (int, int) p) -> (int, int) position)
        public ContainableItem(int id, Name productName, double price, int quantity, (int, int) position) : base(id,productName, price, quantity)
        {
            Position = position;
        }

        public override string ToString()
        {
            return base.ToString() + string.Format($" Position: {Position}]");
        }
    }
}
