﻿// Eugen : Remove unused library references, those are unusful lines

namespace ConsoleVMDictionary
{
    class Category
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public const int hashCoefficient = 7;
        public const int hashNumber = 13;

        public Category(int id, string name)
        {
            ID = id;
            Name = name;
        }

        public override string ToString()
        {
            return string.Format($"ID: {ID} \n Name: {Name}");
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Category))
                return false;
            else
                return Name == ((Category)obj).Name
                    && ID == ((Category)obj).ID;// Eugen : remove the below 2 empty lines
        }

        // Eugen : magic numbers - see comment on Product.cs
        //done
        public override int GetHashCode()
        {
            int hash = hashNumber;
            hash = (hash * hashCoefficient) + ID.GetHashCode();
            hash = (hash * hashCoefficient) + Name.GetHashCode();

            return hash;
        }
    }
}
