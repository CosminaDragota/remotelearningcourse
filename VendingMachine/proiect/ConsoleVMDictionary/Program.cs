﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleVMDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Service service = new Service();
            IProductCollection collection = service.GetCollection();
            bool run = true;
            bool changeQuantity = false;
            int quantity = 0;
            do
            {
                collection.ListCategories();
                Console.Write("\nEnter category id:");
                int categoryID = int.Parse(Console.ReadLine().ToString());

                Category category = collection.GetCategory(categoryID);
                collection.ListProducts(category);


                Console.Write("\n Enter the ID of the product you want to buy:");

                int itemID = int.Parse(Console.ReadLine().ToString());
                (ContainableItem containableItem, double amount) dummy =
                    collection.GetProduct(category, itemID, 1);
                Console.Write("You selected: " + dummy.containableItem.ToString());

                do
                {
                    changeQuantity = false;

                    Console.Write("\n Enter quantity: ");
                    quantity = int.Parse(Console.ReadLine().ToString());

                    if (quantity > dummy.containableItem.Quantity)
                    {
                        Console.WriteLine("Enter a quantity lower than or equal to " + dummy.containableItem.Quantity);
                        changeQuantity = true;
                    }else if(quantity<0)
                    {
                        Console.WriteLine("Enter a positite quantity!");
                        changeQuantity = true;
                    }
                } while (changeQuantity);

                (ContainableItem containableItem, double amount) getInfo =
                    collection.GetProduct(category, itemID, quantity);

                    Console.WriteLine("The amount to pay is: " + getInfo.amount);
                    Console.WriteLine("\n Do you confirm payment? [y/n]");
                    string confirm = Console.ReadLine().ToString();
                if (confirm == "y")
                {
                    collection.DecreaseQuantity(getInfo.containableItem, quantity);
                }
                Console.WriteLine("The updated list of products of category: " + category.Name);
                collection.ListProducts(category);

                Console.WriteLine("Do you want to choose another product? [y/n]");
                string choose = Console.ReadLine().ToString();
                if (choose == "n")
                {
                    run = false;
                }


            } while (run);

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();

        }
    }
}
