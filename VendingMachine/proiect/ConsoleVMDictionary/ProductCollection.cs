﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleVMDictionary
{
    class ProductCollection:IProductCollection
    {
        private Dictionary<Category, HashSet<ContainableItem>> Dictionary { get; set; }
        public const int zero = 0;
        

        public ProductCollection() { Dictionary = new Dictionary<Category, HashSet<ContainableItem>>(); }

        void IProductCollection.AddProduct(ContainableItem item, Category category)
        {
            HashSet<ContainableItem> products = new HashSet<ContainableItem>();
            Dictionary.TryGetValue(category, out products);
            products.Add(item);
        }

        void IProductCollection.ListProducts(Category category)
        {
            HashSet<ContainableItem> products = new HashSet<ContainableItem>();
            Dictionary.TryGetValue(category, out products);
            if (products.Count != zero)
            {
                foreach (Product p in products)
                {
                    Console.WriteLine(p.ToString());
                }
            }
            else
            {
                Console.WriteLine($"The category { category}doesn't have products!");

            }

        }

        public (ContainableItem item, double priceToPay) GetProduct(Category category, int id, int quantity)
        {
            double calculatedPrice = 0.0;
            HashSet<ContainableItem> products = new HashSet<ContainableItem>();
            Dictionary.TryGetValue(category, out products);
            if (products.Count != 0)
            {
                foreach (ContainableItem product in products)
                {
                    if (product.ID == id)
                    {
                        
                        calculatedPrice = CalculatePrice(product.Price, quantity);
                        return (product, calculatedPrice);
                    }
                    
                }
            }
            else
            {
                Console.WriteLine("Product not found!");
            }
            return (null, zero);

        }

        public double CalculatePrice(double price, int quantity)
        {
            return price * quantity;
        }

       void IProductCollection.DecreaseQuantity(Product productToPay, int quantity)
        {
            if (productToPay != null)
            {
                productToPay.Quantity = productToPay.Quantity- quantity;
            }
        }

        void IProductCollection.AddCategory(Category category)
        {
            HashSet<ContainableItem> items = new HashSet<ContainableItem>();
            if (!Dictionary.ContainsKey(category))
            {
                Dictionary.Add(category, items);

                Console.WriteLine($"Category {category.Name} added!\n");
            }
            else
            {
                Console.WriteLine($"Category {category.Name} is already in the list!");
            }

        }

        public void ListCategories()
        {
            Console.WriteLine("Categories:");
            List<Category> categories = Dictionary.Keys.ToList();
            foreach(Category c in categories)
            {
                Console.WriteLine(c.ToString());
            }
        }

        Category IProductCollection.GetCategory(int id)
        {
            List<Category> categories = Dictionary.Keys.ToList();
            foreach (Category c in categories)
            {
                if (c.ID == id)
                {
                    return c;
                }
            }
            return null;
        }

       
    }
    }

