﻿// Eugen : Remove unused library references, those are unusful lines
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleVMDictionary
{

    public enum Name
    {
        Milka, Poiana, Heidi, Africana, Schogetten, Lindt,
        Snickers, Kinder, Lion, Twix, Fitness,
        Pepsi, CocaCola, Tymbark, Fanta, Santal,
        Tchibo, Lavazza, Nescafe, Davidoff
    }

    abstract class Product
    {
        public int ID { get; set; }
        public Name ProductName { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public const int hashCoefficient = 7;
        public const int hashNumber = 13;

        public Product(int id, Name productName, double price, int quantity)
        {
            ID = id;
            ProductName = productName;
            Price = price;
            Quantity = quantity;
        }

        public override string ToString()
        {
            return string.Format($"[ID: {ID}\n Name: {ProductName};\n Price: {Price};\n Quantity: {Quantity};\n ");
        
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Product))
            {
                return false;
            }
            else
            {
                return ID == ((Product)obj).ID
                    && ProductName == ((Product)obj).ProductName
                    && Price == ((Product)obj).Price
                    && Quantity == ((Product)obj).Quantity;
            }
        }

        public override int GetHashCode()
        {
            int hash = hashNumber;
            hash = (hash * hashCoefficient) + ID.GetHashCode();
            hash = (hash * hashCoefficient) + ProductName.GetHashCode();
            hash = (hash * hashCoefficient) + Price.GetHashCode();
            hash = (hash * hashCoefficient) + Quantity.GetHashCode();
       
            return hash;
        }
    }
}
