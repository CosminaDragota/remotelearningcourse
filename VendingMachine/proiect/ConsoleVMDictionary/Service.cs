﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleVMDictionary
{
    class Service
    {
        IProductCollection collection = new ProductCollection();
        int id = 0;
        Category chocolate = new Category(1, "chocolate");
        Category stick = new Category(2, "stick");
        Category soda = new Category(3, "soda");
        Category coffee = new Category(4, "coffee");
        

        public Service()
        {
            PopulateVendingMachine();
        }

        public IProductCollection GetCollection()
        {
            return collection;
        }

        public void PopulateVendingMachine()
        {
            collection.AddCategory(chocolate);
            collection.AddCategory(stick);
            collection.AddCategory(soda);
            collection.AddCategory(coffee);

            collection.AddProduct(new ContainableItem
                (++id, Name.Africana, 2.22, 5, (1, 1)), chocolate);
            collection.AddProduct(new ContainableItem
                (++id, Name.Heidi, 9.4, 5, (1, 2)), chocolate);
            collection.AddProduct(new ContainableItem
                    (++id, Name.Lindt, 14.4, 5, (1, 3)), chocolate);
            collection.AddProduct(new ContainableItem
                    (++id, Name.Milka, 4.0, 5, (1, 4)), chocolate);
            collection.AddProduct(new ContainableItem
                (++id, Name.Poiana, 2.4, 5, (1, 5)), chocolate);
            collection.AddProduct(new ContainableItem
                    (++id, Name.Schogetten, 3.7, 5, (1, 6)), chocolate);
            collection.AddProduct(new ContainableItem
                (++id, Name.Heidi, 6.8, 5, (1, 7)), chocolate);

            collection.AddProduct(new ContainableItem
                    (++id, Name.Snickers, 3.4, 5, (2, 1)), stick);
            collection.AddProduct(new ContainableItem
                (++id, Name.Fitness, 2.4, 5, (2, 2)), stick);
            collection.AddProduct(new ContainableItem
                (++id, Name.Kinder, 2.1, 5, (2, 3)), stick);
            collection.AddProduct(new ContainableItem
                (++id, Name.Lion, 2.3, 5, (2, 4)), stick);
            collection.AddProduct(new ContainableItem
                (++id, Name.Twix, 2.5, 5, (2, 5)), stick);

            collection.AddProduct(new ContainableItem
                (++id, Name.Pepsi, 5.0, 5, (3, 1)), soda);
            collection.AddProduct(new ContainableItem
                (++id, Name.CocaCola, 5.0, 5, (3, 2)), soda);
            collection.AddProduct(new ContainableItem
                (++id, Name.Tymbark, 5.0, 5, (3, 3)), soda);
            collection.AddProduct(new ContainableItem
                (++id, Name.Fanta, 4.8, 5, (3, 4)), soda);
            collection.AddProduct(new ContainableItem
                (++id, Name.Santal, 4.8, 5, (3, 5)), soda);

            collection.AddProduct(new ContainableItem
                (++id, Name.Tchibo, 2.0, 5, (4, 1)), coffee);
            collection.AddProduct(new ContainableItem
                (++id, Name.Lavazza, 2.0, 5, (4, 2)), coffee);
            collection.AddProduct(new ContainableItem
                (++id, Name.Nescafe, 2.0, 5, (4, 3)), coffee);
            collection.AddProduct(new ContainableItem
                (++id, Name.Davidoff, 2.0, 5, (4, 4)), coffee);
        }


    }
}
