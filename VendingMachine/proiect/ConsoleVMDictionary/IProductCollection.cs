﻿// Eugen : Remove unused library references, those are unusful lines
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleVMDictionary
{
    interface IProductCollection
    {
        void AddProduct(ContainableItem item, Category category);
        void ListProducts(Category category);
        (ContainableItem item, double priceToPay) GetProduct(Category category, int id, int quantity);
        void DecreaseQuantity(Product productToPay, int quantity);
        void AddCategory(Category category);
        void ListCategories();
        Category GetCategory(int id);
        double CalculatePrice(double price, int quantity);
    }
}
