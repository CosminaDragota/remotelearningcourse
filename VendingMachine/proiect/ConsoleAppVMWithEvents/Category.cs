﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Balazs : Remove unused library references, those are un-useful lines

namespace ConsoleAppVMWithEvents
{
    public class Category
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public const int HashNumber = 13;
        public const int HashCoefficient = 7;

        public Category(int id, string name)
        {
            ID = id;
            Name = name;
        }

        public override string ToString()
        {
            return string.Format($"ID: {ID} \n Name: {Name}");
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Category))
            {
                return false;
            }
            else
            {
                return Name == ((Category)obj).Name
                    && ID == ((Category)obj).ID;
            }
        }

        public override int GetHashCode()
        {
            int hash = HashNumber;
            hash = (hash * HashCoefficient) + ID.GetHashCode();
            hash = (hash * HashCoefficient) + Name.GetHashCode();

            return hash;
        }

    }
}
