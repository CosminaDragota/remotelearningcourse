﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;

namespace ConsoleAppVMWithEvents
{
    // Balazs : rename class, it is too generic
    public class Service
    {
        ProductCollection collection = new ProductCollection();
        int id = 0;
        Category chocolate = new Category(1, "chocolate");
        Category stick = new Category(2, "stick");
        Category soda = new Category(3, "soda");
        Category coffee = new Category(4, "coffee");


        public Service()
        {
            PopulateVendingMachineWithDelegates();
        }

        public ProductCollection GetCollection()
        {
            return collection;
        }

        public string ConvertToJson(List<Category> categories)
        {
            string JSONResponse = JsonConvert.SerializeObject(categories);
            return JSONResponse;
        }
        

        public void PopulateVendingMachineWithDelegates()
        {
            collection.AddCategoryAction(chocolate);
            collection.AddCategoryAction(stick);
            collection.AddCategoryAction(soda);
            collection.AddCategoryAction(coffee);

            // Balazs : maybe AddProduct could handle id generation behind the scenes
            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Africana, 2.22, 5, (1, 1)), chocolate);
            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Heidi, 9.4, 5, (1, 2)), chocolate);
            collection.AddProductAction(new ContainableItem
                    (++id, ProductName.Lindt, 14.4, 5, (1, 3)), chocolate);
            collection.AddProductAction(new ContainableItem
                    (++id, ProductName.Milka, 4.0, 5, (1, 4)), chocolate);
            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Poiana, 2.4, 5, (1, 5)), chocolate);
            collection.AddProductAction(new ContainableItem
                    (++id, ProductName.Schogetten, 3.7, 5, (1, 6)), chocolate);
            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Heidi, 6.8, 5, (1, 7)), chocolate);

            collection.AddProductAction(new ContainableItem
                    (++id, ProductName.Snickers, 3.4, 5, (2, 1)), stick);
            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Fitness, 2.4, 5, (2, 2)), stick);
            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Kinder, 2.1, 5, (2, 3)), stick);
            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Lion, 2.3, 5, (2, 4)), stick);
            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Twix, 2.5, 5, (2, 5)), stick);

            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Pepsi, 5.0, 5, (3, 1)), soda);
            collection.AddProductAction(new ContainableItem
                (++id, ProductName.CocaCola, 5.0, 5, (3, 2)), soda);
            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Tymbark, 5.0, 5, (3, 3)), soda);
            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Fanta, 4.8, 5, (3, 4)), soda);
            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Santal, 4.8, 5, (3, 5)), soda);

            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Tchibo, 2.0, 5, (4, 1)), coffee);
            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Lavazza, 2.0, 5, (4, 2)), coffee);
            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Nescafe, 2.0, 5, (4, 3)), coffee);
            collection.AddProductAction(new ContainableItem
                (++id, ProductName.Davidoff, 2.0, 5, (4, 4)), coffee);
        }

    }
}
