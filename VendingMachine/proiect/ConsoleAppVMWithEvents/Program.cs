﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ConsoleAppVMWithEvents
{
    public delegate List<Category> ListCategoriesDelegate();

    // Balazs : program class, main method contains/does too many things, it should just start the vending machine
    // Methods/functionalities of a vending machine should be defined in an interface.
    public static class Program
    {

        // Balazs : logger should not be in Program class
        private static readonly log4net.ILog log
       = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Balazs : checking input is UI related functionality
        private const string AnswerRegex = @"^[0-9*#+]+$";
        private const string QuantityRegex = @"^[a-z]*$";
        // Balazs : checking if payment is valid is not UI functinality, separate these
        private static bool validPayment = false;
        private static bool run = true;
        private static bool idAnswer = true;
        private static bool jsonAnswer = true;
        private static bool productIdAnswer = true;
        private static bool changeQuantity = false;

        private static bool ValidPayment { get => validPayment; set => validPayment = value; }
        private static bool Run { get => run; set => run = value; }
        private static bool IdAnswer { get => idAnswer; set => idAnswer = value; }
        private static bool JsonAnswer { get => jsonAnswer; set => jsonAnswer = value; }
        private static bool ProductIdAnswer { get => productIdAnswer; set => productIdAnswer = value; }
        private static bool ChangeQuantity { get => changeQuantity; set => changeQuantity = value; }


        static void CheckQuantity(object sender, QuantityValidEventArgs eventArgs)
        {

            if (eventArgs.Quantity > 0)
            {
                Console.WriteLine($"There are {eventArgs.Quantity} left on stock!");
            }
            else
            {
                Console.WriteLine("Out of stock!");
            }
        }

        // Balazs : main method should just start the vending machine
        static void Main(string[] args)
        {
            Service productService = new Service();
            ProductCollection collection = productService.GetCollection();
            ListCategoriesDelegate delegateListCategories = collection.ListCategories;
            Category category = null;
            int quantity = 0;
            bool isPaid = false;
            //Balazs : dummy? maybe selectedProduct. Dummy does not tell what that variable contains.
            (ContainableItem containableItem, double amount) dummy; 
            do
            {
                JsonService(productService, delegateListCategories);
                category = ChooseCategory(collection);
                dummy = ChooseProduct(category, collection);
                quantity = ChooseQuantity(dummy);

                dummy.containableItem.IsQuantityValidEvent += CheckQuantity;
                (ContainableItem containableItem, double amount) getInfo =
                    collection.GetProductFunc(category, dummy.containableItem.ID, quantity);

                // Balazs : create an interface and an implementation for it which uses Console.Writeline to show messages.
                // logic should not care about how messages are shown.
                // Is valid for every Console.WriteLine from this class.
                Console.WriteLine("\n\nThe amount to pay is: " + getInfo.amount);

                isPaid = PayProduct();

                if (isPaid)
                {
                    collection.DecreaseQuantityAction(getInfo.containableItem, quantity);
                    AskUser();
                    collection.ListProductsAction(category);
                    Console.WriteLine($"\nAbove is The updated list of products of category: {category.Name}\n Press any key to exit.");
                }

            } while (Run);

            collection.ListProductsAction(category);
            Console.WriteLine("\n Press any key to exit.");
            Console.ReadKey();
        }

        public static void AskUser()
        {
            try
            {
                Console.WriteLine("\nDo you want to choose another product? [y/n]");
                string choose = Console.ReadLine().ToString();
                if (choose == "n")
                {
                    Run = false;
                }
                else if (choose == "y")
                {
                    Run = true;
                    ValidPayment = false;
                }
                else if (Regex.IsMatch(choose, AnswerRegex))
                {
                    throw new FormatException();
                }
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Choose a letter: y/n");
                log.Warn(ex.Message);
            }
            catch (Exception ex)
            {
                log.Warn(ex.Message);

            }

            // Balazs : remove empty lines
        }

        public static Category ChooseCategory(ProductCollection collection)
        {
            Category category = null;
            int categoryID = 0;
            do
            {
                // Balazs : remove empty line, I won't add this comments for all occurence, but please fix each.
                try
                {

                    Console.Write("\nEnter category id from 1-4:");
                    int.TryParse(Console.ReadLine().ToString(), out categoryID);

                    if (categoryID <= 0 || categoryID > 4)
                    {
                        IdAnswer = true;
                        throw new ArgumentOutOfRangeException();
                    }
                    else if (categoryID >= 1 && categoryID <= 4) // Balazs : what does 1 and 4 mean? magic numbers?
                    {
                        IdAnswer = false;
                        category = collection.GetCategoryFunc(categoryID);
                        collection.ListProductsAction(category);
                        // Balazs : remove empty line
                    }

                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine("Id not valid!");
                    log.Info(ex.Message);
                }
                catch (Exception ex)
                {
                    log.Warn(ex.Message);
                }
            } while (IdAnswer);
            return category;
        }

        // Balazs : rename method, to clearly see what it does
        public static void JsonService(Service productService, ListCategoriesDelegate delegateListCategories)
        {
            List<Category> categories = delegateListCategories();
            do
            {
                try
                {
                    Console.WriteLine("Do you want to convert the category list into JSON? [y/n]");
                    string answer = Console.ReadLine().ToString(); // Balazs : no need for ToString()
                    if (answer == "y") // Balazs : y and n magic letters :), extract as constants Yes and False
                    {
                        Console.WriteLine(productService.ConvertToJson(categories));
                        JsonAnswer = false;

                    }
                    if (answer == "n")
                    {
                        JsonAnswer = false;
                    }
                    else if (Regex.IsMatch(answer, AnswerRegex))
                    {
                        JsonAnswer = true;
                        throw new FormatException();

                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Choose a letter: y/n");
                    log.Warn(ex.Message);
                }
            } while (JsonAnswer);

        }

        public static (ContainableItem containableItem, double amount) ChooseProduct(Category category, ProductCollection collection)
        {
            int itemID = 0;
            // Balazs : rename dummy, see comment about this above, line 59
            (ContainableItem containableItem, double amount) dummy = collection.GetProductFunc(collection.GetCategoryFunc(1), itemID, 1);

            do
            {
                try
                {
                    Console.Write("\n Enter the ID of the product you want to buy:");

                    int.TryParse(Console.ReadLine().ToString(), out itemID);
                    dummy =
                        collection.GetProductFunc(category, itemID, 1);
                    if (dummy.containableItem == null)
                    {
                        ProductIdAnswer = true;
                        throw new ArgumentOutOfRangeException();
                    }
                    else
                    {
                        Console.Write("\n\nYou selected: " + dummy.containableItem.ToString());
                        ProductIdAnswer = false;
                    }
                }catch(ArgumentOutOfRangeException e) // Balazs : use spaces after bracket and before catch
                {
                    Console.WriteLine("Choose a valid id:");
                    log.Info(e.Message);
                }

            } while (ProductIdAnswer);

            return dummy;
        }

        public static int ChooseQuantity((ContainableItem containableItem, double amount) dummy)
        {
            int quantity = 0;
            do
            {
                try
                {


                    changeQuantity = false;

                    Console.Write("\n\n Enter quantity: ");
                    int.TryParse(Console.ReadLine().ToString(), out quantity);
                    Console.WriteLine(string.Format($" quantity input:  {quantity.ToString()}"));
                    Console.WriteLine(string.Format($"dummy quantity: {dummy.containableItem.Quantity}"));

                    if (Regex.IsMatch(quantity.ToString(), QuantityRegex))
                    {
                        changeQuantity = true;
                        throw new FormatException();

                    }
                    else if (quantity > dummy.containableItem.Quantity || quantity < 0)
                    {
                        changeQuantity = true;
                        throw new ArgumentOutOfRangeException();
                    }
                }
                catch (FormatException e)
                {
                    Console.WriteLine("Enter a number!");
                    log.Warn(e.Message);

                }
                catch (ArgumentOutOfRangeException e)
                {
                    Console.WriteLine($"\nEnter a positive quantity, lower than or equal to {dummy.containableItem.Quantity}");
                    log.Info(e.Message);
                }


            } while (changeQuantity);
            return quantity;
        }

        public static bool PayProduct()
        {
            bool repeat = true;
            do
            {
                try
                {


                    Console.WriteLine("\n Do you confirm payment? [y/n]");
                    string confirm = Console.ReadLine().ToString();
                    if (confirm == "y")
                    {

                        ValidPayment = true;
                        repeat = false;
                    }
                    else if (confirm == "n")
                    {
                        repeat = false;

                        AskUser();

                        // Balazs : again too many empty lines
                    }
                    else if (Regex.IsMatch(confirm, AnswerRegex))
                    {

                        Run = true;
                        repeat = true;
                        throw new FormatException();
                    }
                }catch(FormatException e)
                {
                    Console.WriteLine("Choose a letter: y/n");
                    log.Warn(e.Message);
                }
            } while (repeat);

            return ValidPayment;
        }

    }
}
