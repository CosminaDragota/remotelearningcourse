﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppVMWithEvents
{
    public class ProductCollection
    {
            public const int Zero = 0;
            private static Dictionary<Category, HashSet<ContainableItem>> Dictionary { get; set; }

            public ProductCollection()
            {
                Dictionary = new Dictionary<Category,
                                            HashSet<ContainableItem>>();
            }

            public Action<ContainableItem, Category> AddProductAction = (ContainableItem item, Category category) =>
            {
                HashSet<ContainableItem> products = new HashSet<ContainableItem>();
                Dictionary.TryGetValue(category, out products);
                products.Add(item);
            };

            public Action<Category> ListProductsAction = (Category category) =>
            {
                HashSet<ContainableItem> products = new HashSet<ContainableItem>();
                Dictionary.TryGetValue(category, out products);
                if (products.Count != Zero)
                {
                    foreach (Product product in products)
                    {
                        Console.WriteLine(product.ToString());
                    }
                }
                else
                {
                    Console.WriteLine($"The category {category} doesn't have products!");
                }
            };

            // Balazs : create a Class from this tuple, something like a Receipt
            public Func<Category, int, int, (ContainableItem, double)> GetProductFunc =
                (Category category, int id, int quantity) =>
                {
                double calculatedPrice = Zero;
                    HashSet<ContainableItem> products = new HashSet<ContainableItem>();
                    Dictionary.TryGetValue(category, out products);
                    if (products.Count != Zero)
                    {
                    foreach (ContainableItem product in products)
                        {
                            if (product.ID == id)
                            {

                                calculatedPrice = CalculatePriceFunc(product.Price, quantity);
                                return (product, calculatedPrice);
                            }
                    }// Balazs : indentation of Foreach in If is wrong, brackets alignment is also wrong
                    }
                    else
                    {
                        Console.WriteLine("Product not found!");
                    }
                    return (null, Zero);
            };

            public Action<Product, int> DecreaseQuantityAction = (Product productToPay, int quantity) =>
            {
                if (productToPay != null)
                {
                    productToPay.Quantity = productToPay.Quantity - quantity;
                }
            };

            public Action<Category> AddCategoryAction = (Category category) =>
            {
                HashSet<ContainableItem> items = new HashSet<ContainableItem>();
                if (!Dictionary.ContainsKey(category))
                {
                    Dictionary.Add(category, items);

                    Console.WriteLine($"Category { category.Name} added!\n");
                }
                else
                {
                    Console.WriteLine($"Category {category.Name} is already in the list!");
                }
            };


            public List<Category> ListCategories()
            {
                Console.WriteLine("Categories:");
                List<Category> categories = Dictionary.Keys.ToList();
                foreach (Category c in categories)
                {
                    Console.WriteLine(c.ToString());
                }
            return categories;
            }

            public Func<int, Category> GetCategoryFunc = (int id) =>
            {
                List<Category> categories = Dictionary.Keys.ToList();
                foreach (Category category in categories)
                {
                    if (category.ID == id)
                    {
                        return category;
                    }
                }
                return null;
            };

            public static Func<double, int, double> CalculatePriceFunc = (double price, int quantity) =>
            {
                return price * quantity;

            };
        }


    
}
