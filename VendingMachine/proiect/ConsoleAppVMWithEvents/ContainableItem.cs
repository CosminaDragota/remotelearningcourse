﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Balazs : Remove unused library references, those are un-useful lines

namespace ConsoleAppVMWithEvents
{

    public class ContainableItem : Product
    {
        readonly (int, int) Position;

        public ContainableItem(int id, ProductName productName, double price, int quantity, (int, int) position) : base(id, productName, price, quantity)
        {
            Position = position;
        }

        public override string ToString()
        {
            return base.ToString() + string.Format($" Position: {Position}]");
        }
    }

}
