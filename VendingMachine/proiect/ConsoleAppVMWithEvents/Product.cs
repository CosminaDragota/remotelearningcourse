﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Balazs : Remove unused library references, those are un-useful lines

namespace ConsoleAppVMWithEvents
{
    public enum ProductName
    {
        Milka, Poiana, Heidi, Africana, Schogetten, Lindt,
        Snickers, Kinder, Lion, Twix, Fitness,
        Pepsi, CocaCola, Tymbark, Fanta, Santal,
        Tchibo, Lavazza, Nescafe, Davidoff
    }

    // Balazs : move to separate .cs file. One file should contain only 1 class, and the class name should be the same as file name.
    public class QuantityValidEventArgs : EventArgs
    {
        public int Quantity { get; private set; }
        // Balazs : setter not used, you can remove it

        public QuantityValidEventArgs(int quantity)
        {
            Quantity = quantity;
        }
    }

    public delegate void IsQuantityValidDelegate(object sender, QuantityValidEventArgs eventArgs);

    public abstract class Product
    {
        public const int HashNumber = 13;
        public const int HashCoefficient = 7;

        public IsQuantityValidDelegate IsQuantityValidDelegate { get; set; }
        public event IsQuantityValidDelegate IsQuantityValidEvent;
        private int quantity;

        public int ID { get; set; }
        public ProductName ProductName { get; set; }
        public double Price { get; set; }
        internal int Quantity
        {
            get => quantity;
            set
            {
                quantity = value;
                if (quantity > 0) 
                {
                    QuantityValidEventArgs eventArgs = new QuantityValidEventArgs(quantity);
                    IsQuantityValidEvent?.Invoke(this, eventArgs);
                }
            }
        }

        protected Product(int id, ProductName productName, double price, int quantity)
        {
            ID = id;
            ProductName = productName;
            Price = price;
            Quantity = quantity;
        }

        public override string ToString()
        {
            return string.Format($"[ID:{ID} Name: {ProductName};\n Price: {Price};\n Quantity: {Quantity};\n ");
        }


        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Product))
            {
                return false;
            }
            else
            {
                return ID == ((Product)obj).ID
                    && ProductName == ((Product)obj).ProductName
                    && Price == ((Product)obj).Price
                    && Quantity == ((Product)obj).Quantity;
            }
        }

        public override int GetHashCode()
        {
            int hash = HashNumber;
            hash = (hash * HashCoefficient) + ID.GetHashCode();
            hash = (hash * HashCoefficient) + ProductName.GetHashCode();
            hash = (hash * HashCoefficient) + Price.GetHashCode();
            hash = (hash * HashCoefficient) + Quantity.GetHashCode();

            return hash;
        }
    }
}

