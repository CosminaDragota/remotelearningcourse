﻿// Eugen : Remove unused library references, those are unusful lines
//done
namespace ConsoleAppVMWithDelegates
{// Eugen : why is line 11 empty
    public enum ProductName
    {
        // Eugen : the name of this enum is way to generic,
        // use something like ProductName 
        // look at the line :  public Name ProductName { get; set; }
        // there the name of the property for this enum is correct, 
        // that should be also the name of the enum
        //done
        Milka, Poiana, Heidi, Africana, Schogetten, Lindt,
        Snickers, Kinder, Lion, Twix, Fitness,
        Pepsi, CocaCola, Tymbark, Fanta, Santal,
        Tchibo, Lavazza, Nescafe, Davidoff
    }

    public abstract class Product
    {
        public int ID { get; set; }
        public ProductName ProductName { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public const int hashNumber = 13;
        public const int hashCoefficient = 7;

        // Eugen : this constructor should be protected
        // because it is an abstract class, it cannot be instantiated and to use it
        // you will always need to implement this abstract class
        //done
        protected Product(int id, ProductName productName, double price, int quantity)
        {
            ID = id;
            ProductName = productName;
            Price = price;
            Quantity = quantity;
        }

        public override string ToString()
        {
            // Eugen : use string interpolation
            //done
            return string.Format($"[ID:{ID} Name: {ProductName};\n Price: {Price};\n Quantity: {Quantity};\n ");
        
        }

        // Eugen : use {} to mark the scope for each of the below conditions in the "if"
        //done
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Product))
            {
                return false;
            }
            else
            {
                return ID == ((Product)obj).ID
                    && ProductName == ((Product)obj).ProductName
                    && Price == ((Product)obj).Price
                    && Quantity == ((Product)obj).Quantity;
            }
                

        }

        // Eugen : 13, 7 are magic numbers 
        // those are used in this app in Category.cs and Product.cs
        // you can either extract them in the header of the classes or even better
        // declare an enum for the entire app that has for its options the values of 13 and 7 
        //done
        public override int GetHashCode()
        {
            int hash = hashNumber;
            hash = (hash * hashCoefficient) + ID.GetHashCode();
            hash = (hash * hashCoefficient) + ProductName.GetHashCode();
            hash = (hash * hashCoefficient) + Price.GetHashCode();
            hash = (hash * hashCoefficient) + Quantity.GetHashCode();
        
            return hash;
        }
    }
}
