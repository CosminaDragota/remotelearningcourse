﻿// Eugen : Remove unused library references, those are unusful lines
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppVMWithDelegates
{
    class ProductCollection
    {
        public const int zero = 0;
        private static Dictionary<Category, HashSet<ContainableItem>> Dictionary { get; set; }

        public ProductCollection()
        {
            Dictionary = new Dictionary<Category,
                                        HashSet<ContainableItem>>();
        }

        // Eugen : good usage of Action anbd Func
        public Action<ContainableItem, Category> AddProductAction = (ContainableItem item, Category category) =>
          {
              HashSet<ContainableItem> products = new HashSet<ContainableItem>();
              Dictionary.TryGetValue(category, out products);
              products.Add(item);
          };// Eugen : below is an empty line, keep only one line between the methods

        public Action<Category> ListProductsAction = (Category category) =>
          {
              HashSet<ContainableItem> products = new HashSet<ContainableItem>();
              Dictionary.TryGetValue(category, out products);

              // Eugen : "0" is a magic number, address this issue
              //done
              if (products.Count != zero)
              {
                  // Eugen Product "p" -> Product product
                  //done
                  foreach (Product product in products)
                  {
                      Console.WriteLine(product.ToString());
                  }
              }
              else
              {
                  // Eugen : use string iterpolation with string.Format
                  // after this C.W(), you have an empty space
                  //done
                  Console.WriteLine($"The category {category} doesn't have products!");

              }
          };

        public Func<Category, int, int, (ContainableItem, double)> GetProductFunc =
            (Category category, int id, int quantity) =>
            {
                // Eugen : 0 and 0.0 are magic numbers
                double calculatedPrice = zero;
                HashSet<ContainableItem> products = new HashSet<ContainableItem>();
                Dictionary.TryGetValue(category, out products);
                if (products.Count != zero)
                {
                    // Eugen : "p"...
                    //done
                    foreach (ContainableItem product in products)
                    {
                        if (product.ID == id)
                        {

                            calculatedPrice = CalculatePriceFunc(product.Price, quantity);
                            return (product, calculatedPrice);
                        }// Eugen : empty space below
                    }
                }
                else
                {
                    Console.WriteLine("Product not found!");
                }
                return (null, zero);// Eugen : empty space below
            };

        public Action<Product, int> DecreaseQuantityAction = (Product productToPay, int quantity) =>
          {
              if (productToPay != null)
              {
                  productToPay.Quantity = productToPay.Quantity - quantity;
              }
          };

        public Action<Category> AddCategoryAction = (Category category) =>
          {
              HashSet<ContainableItem> items = new HashSet<ContainableItem>();
              if (!Dictionary.ContainsKey(category))
              {
                  Dictionary.Add(category, items);

                  Console.WriteLine($"Category { category.Name} added!\n");
              }
              else
              {
                  Console.WriteLine($"Category {category.Name} is already in the list!");
              }
          };


        public void ListCategories()
        {
            Console.WriteLine("Categories:");
            List<Category> categories = Dictionary.Keys.ToList();
            foreach (Category c in categories)
            {
                Console.WriteLine(c.ToString());
            }
        }

        public Func<int, Category> GetCategoryFunc = (int id) =>
          {
              List<Category> categories = Dictionary.Keys.ToList();
              foreach (Category category in categories)
              {
                  if (category.ID == id)
                  {
                      return category;
                  }
              }
              return null;
          };

        public static Func<double, int, double> CalculatePriceFunc=(double price, int quantity) =>
            {
                return price * quantity;

            };
        }


    }

