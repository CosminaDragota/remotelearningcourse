﻿// Eugen : Remove unused library references, those are unusful lines
//done
using System;


namespace ConsoleAppVMWithDelegates
{

    delegate void ListCategoriesDelegate();
    class Program
    {

        static void Main(string[] args)
        {
            Service service = new Service();
            ProductCollection collection = service.GetCollection();

            // Eugen : "del" is a name that is not telling what this is for
            // use a more explicit name to point what does this variable is doing
            //done
            ListCategoriesDelegate delegateListCategories = collection.ListCategories;
            bool changeQuantity = false;
            int quantity = 0;
            bool run = true;
            do
            {
                delegateListCategories();
                Console.Write("\nEnter category id:");
                int categoryID = int.Parse(Console.ReadLine().ToString());

                Category category = collection.GetCategoryFunc(categoryID);
                collection.ListProductsAction(category);

                Console.Write("\n Enter the ID of the product you want to buy:");

                int itemID = int.Parse(Console.ReadLine().ToString());
                (ContainableItem containableItem, double amount) dummy =
                    collection.GetProductFunc(category, itemID, 1);
                Console.Write("\n\nYou selected: " + dummy.containableItem.ToString());

                do
                {
                    changeQuantity = false;

                    Console.Write("\n\n Enter quantity: ");
                    quantity = int.Parse(Console.ReadLine().ToString());

                    if (quantity > dummy.containableItem.Quantity)
                    {
                        Console.WriteLine($"\nEnter a quantity lower than or equal to {dummy.containableItem.Quantity}");
                        changeQuantity = true;
                    }
                    else if (quantity < 0)
                    {
                        Console.WriteLine("\nEnter a positive quantity!");
                        changeQuantity = true;
                    }
                } while (changeQuantity);

                (ContainableItem containableItem, double amount) getInfo =
                    collection.GetProductFunc(category, itemID, quantity);

                Console.WriteLine("\n\nThe amount to pay is: " + getInfo.amount);
                Console.WriteLine("\n Do you confirm payment? [y/n]");
                string confirm = Console.ReadLine().ToString();
                if (confirm == "y")
                {
                    collection.DecreaseQuantityAction(getInfo.containableItem, quantity);
                }
                Console.WriteLine("\n\nThe updated list of products of category: " + category.Name);
                collection.ListProductsAction(category);

                Console.WriteLine("\nDo you want to choose another product? [y/n]");
                string choose = Console.ReadLine().ToString();
                if (choose == "n")
                {
                    run = false;
                }
            } while (run);

            Console.WriteLine("\nPress any key to exit.");
            Console.ReadKey();
        }
    }
}
