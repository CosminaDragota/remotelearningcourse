﻿// Eugen : Remove unused library references, those are unusful lines
//done
namespace ConsoleAppVMWithDelegates
{
    // Eugen : use explicit acces modifier
    public class ContainableItem:Product
    {
        public (int, int) Position;

        // Eugen : (int, int) p, replace "p" with explicit word
        public ContainableItem(int id, ProductName productName, double price, int quantity, (int, int) position) : base(id,productName, price, quantity)
        {
            Position = position;
        }

        public override string ToString()
        {
            // Eugen : use string interpolation
            return base.ToString() + string.Format($" Position: {Position}]");
        }
    }
}
