﻿// Eugen : Remove unused library references
//done
namespace ConsoleAppVMWithDelegates
{
    class Category
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public const int hashNumber = 13;
        public const int hashCoefficient = 7;

        public Category(int id, string name)
        {
            ID = id;
            Name = name;
        }

        public override string ToString()
        {
            // Eugen : use string interpolation
            //done
            return string.Format($"ID: {ID} \n Name: {Name}");
        }

        public override bool Equals(object obj)
        {
            // use {} to hightligth scope even if you have for an "if" condition only one instruction (line 30)
            //done
            if (obj == null || !(obj is Category))
            {
                return false;
            }
            else
            {
                return Name == ((Category)obj).Name
                    && ID == ((Category)obj).ID;
            }
        }

        public override int GetHashCode()
        {
            // Eugen : 13, 7 are magic numbers - extract them in the header of the class as constants
            //done
            int hash = hashNumber;
            hash = (hash * hashCoefficient) + ID.GetHashCode();
            hash = (hash * hashCoefficient) + Name.GetHashCode();

            return hash;
        }
    }
}
