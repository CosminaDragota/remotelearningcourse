namespace ConsoleAppCodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditTransactionDescription : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Transactions", "Description", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transactions", "Description", c => c.String());
        }
    }
}
