namespace ConsoleAppCodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditTransaction : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Transactions", new[] { "ProductId" });
            AddColumn("dbo.Transactions", "RefillVendingMachine", c => c.Boolean(nullable: false));
            AddColumn("dbo.Transactions", "BuyProduct", c => c.Boolean(nullable: false));
            AddColumn("dbo.Transactions", "ProductPrice", c => c.Double(nullable: false));
            AlterColumn("dbo.Transactions", "TransactionDate", c => c.DateTime(nullable: false));
            CreateIndex("dbo.Transactions", "ProductID");
            DropColumn("dbo.Transactions", "Description");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transactions", "Description", c => c.Int(nullable: false));
            DropIndex("dbo.Transactions", new[] { "ProductID" });
            AlterColumn("dbo.Transactions", "TransactionDate", c => c.DateTime());
            DropColumn("dbo.Transactions", "ProductPrice");
            DropColumn("dbo.Transactions", "BuyProduct");
            DropColumn("dbo.Transactions", "RefillVendingMachine");
            CreateIndex("dbo.Transactions", "ProductId");
        }
    }
}
