namespace ConsoleAppCodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditTransaction1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Transactions", "ProductPrice");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transactions", "ProductPrice", c => c.Double(nullable: false));
        }
    }
}
