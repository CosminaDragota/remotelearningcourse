﻿using ConsoleAppCodeFirst.BL;
using ConsoleAppCodeFirst.DB.DTO;
using System;
using System.Collections.Generic;

namespace ConsoleAppCodeFirst.Controllers
{
    public class VendingMachineFlowControls
    {
        public static VendingMachineService vendingMachineService = new VendingMachineService();
        public static LogTransactionService logTransactionService = new LogTransactionService();
        public static ReportService reportService = new ReportService();
        public static ProductDTO selectedProduct = null;

        public ProductDTO SelectProductFlow(int productId)
        {
            try
            {
                selectedProduct = vendingMachineService.SelectProduct(productId);
            }
            catch (MyException e)
            {
                throw;
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw;
            }
            return selectedProduct;
        }

        public (bool, string, double) PayProductFlow(double insertedSum, double amountToPay)
        {
            bool isPaid = false;
            string message = null;
            double change = 0.0;
            try
            {
                (isPaid, message, change) = vendingMachineService.PayProduct(insertedSum, amountToPay);
                if (isPaid)
                {
                    vendingMachineService.productService.DecreaseQuantity(selectedProduct);
                    logTransactionService.AddProductTransaction(selectedProduct);
                    return (true, message, change);
                }
                else
                {
                    return (false, message, change);
                }
            }
            catch (MyException e)
            {
                throw;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool RefillQuantitiesAndMoneyFlow()
        {
            bool isQuantityRefilled = false;
            try
            {
                isQuantityRefilled = vendingMachineService.refillService.RefillQuantities();
                vendingMachineService.refillService.RefillSum();
                logTransactionService.AddRefillTransaction();
            }
            catch (Exception e)
            {
                throw;
            }

            return isQuantityRefilled;
        }

        public List<ProductDTO> ListProductsFlow()
        {
            List<ProductDTO> list = vendingMachineService.productService.ListProducts();
            return list;
        }

        public List<TransactionDTO> AllSalesReportFlow()
        {
            List<TransactionDTO> transactions = null;
            try
            {
                transactions = reportService.AllSalesReport();
            }
            catch(Exception e)
            {
                throw;
            }
            return transactions;
        }

        public int RefillsReportFlow()
        {
            try
            {
                return reportService.NumberOfRefillsReport();

            }catch(Exception e)
            {
                throw;
            }
        }
    }
}
