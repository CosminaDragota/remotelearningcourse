﻿using System;
using ConsoleAppCodeFirst.DB.DTO;
using System.Collections.Generic;
using ConsoleAppCodeFirst.UI;
using ConsoleAppCodeFirst.BL;
using System.Text;

namespace ConsoleAppCodeFirst.Controllers
{
    public class VendingMachineController
    {
        private static ConsoleUI ui = new ConsoleUI(); 
        private static VendingMachineFlowControls flowControls = new VendingMachineFlowControls();
        private static ProductDTO dto = null;
        private static StringBuilder stringBuilder = null;

        public string OptionOne()
        {
            stringBuilder = new StringBuilder();
            List<ProductDTO> list = flowControls.ListProductsFlow();

            foreach(ProductDTO product in list)
            {
                stringBuilder.AppendLine(product.ToString());
            }
            return stringBuilder.ToString();
        }

        public string OptionTwo(int productId)
        {
            stringBuilder = new StringBuilder();

            try
            {
                dto = flowControls.SelectProductFlow(productId);
                if (dto == null)
                {
                     stringBuilder.AppendLine("Select another product!");
                }
                else
                {
                    stringBuilder.AppendLine($"You selected: \n {dto.ToString()}");
                    stringBuilder.AppendLine($"The amount to pay is: {dto.Price}");
                }
            }
            catch (MyException e)
            {
                stringBuilder.Clear();
                stringBuilder.Append(e.Message);
            }
            catch (ArgumentOutOfRangeException e)
            {
                stringBuilder.Clear();
                stringBuilder.Append(e.Message);
            }
            return stringBuilder.ToString();
        }

        public string OptionThree(double insertedSum)
        {
            bool isPaid = false;
            string message = null;
            double change = 0.0;
            stringBuilder = new StringBuilder();

            try
            {
                if (dto != null)
                {
                    (isPaid, message, change) = flowControls.PayProductFlow(insertedSum, dto.Price);

                    stringBuilder.AppendLine($"Product {dto.Name} paid successfully!");
                    stringBuilder.AppendLine(message);
                    stringBuilder.AppendLine($" Your change is {change}!");
                }
                else
                {
                    throw new MyException("You must select a product first!");
                }
            }
            catch (MyException e)
            {
                stringBuilder.Clear();
                stringBuilder.AppendLine(e.Message);
            }
            catch (Exception e)
            {
                stringBuilder.Clear();
                stringBuilder.AppendLine(e.Message);
            }
            return stringBuilder.ToString();
        }

        public string OptionFour()
        {
            bool success = false;
            try
            {
                success = flowControls.RefillQuantitiesAndMoneyFlow();
                return "Refilled successfully!";
            }
            catch (Exception e)
            {
                return "Refill failed!";
            }
        }

        public string OptionFive()
        {
            List<TransactionDTO> allSales = null;
            stringBuilder = new StringBuilder();

            try
            {
                allSales = flowControls.AllSalesReportFlow();

                foreach (TransactionDTO transaction in allSales)
                {
                   stringBuilder.AppendLine((transaction.ToString()));
                }
            }
            catch (Exception e)
            {
                stringBuilder.Clear();
                stringBuilder.AppendLine("Could not generate report!");
            }
            return stringBuilder.ToString();
        }

        public string OptionSix()
        {
            int refills = 0;
            string result;

            try
            {
                refills = flowControls.RefillsReportFlow();
                result = $"The vending machine was refilled {refills} times!";
            }
            catch (Exception e)
            {
                result = "Could not generate report!";
            }
            return result;
        }

        public void StartMainFlow()
        {
            int actionId = 0;
            do
            {
                actionId = ui.DisplayMenu(null);

                switch (actionId)
                {
                    case 1:
                        OptionOne();
                        break;
                    case 2:
                        int productId = ui.ProductInput();
                        OptionTwo(productId);
                        break;
                    case 3:
                        double insertedSum = ui.PaymentInput();
                        OptionThree(insertedSum);
                        break;
                    case 4:
                        OptionFour();
                        break;
                    case 5:
                        OptionFive();
                        break;
                    case 6:
                        OptionSix();
                        break;
                    default:
                        ui.ShowDefaultOption(null);
                        break;
                }
            } while (ui.CanDisplayMenu());

            ui.ShowGoodbyeMessage();
        }

        public string StartVendingMachine(int actionId, int productId, double insertedSum)
        {
            string message;

            switch (actionId)
            {
                case 1:
                    message= OptionOne();
                    break;
                case 2:
                    message = OptionTwo(productId);
                    break;
                case 3:
                    message = OptionThree(insertedSum);
                    break;
                case 4:
                    message = OptionFour();
                    break;
                case 5:
                    message = OptionFive();
                    break;
                case 6:
                    message = OptionSix();
                    break;
                default:
                    message = "Select a valid option";
                    break;
            }
            return message;
        } 

    }
}
