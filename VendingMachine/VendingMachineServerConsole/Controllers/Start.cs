﻿using System.Threading;
using System;
using System.Net;
using System.Net.Sockets;
using ConsoleAppCodeFirst.UI;

namespace ConsoleAppCodeFirst.Controllers
{
    public class Start
    {
        private static VendingMachineController vendingMachineController = new VendingMachineController();
        private static ConsoleUI ui = new ConsoleUI();

        static void Main(string[] args)
        {
            int port = 8001;
            string IpAddress = "127.0.0.1";
            Socket ServerListener = new Socket(AddressFamily
                .InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint ep = new IPEndPoint(IPAddress.Parse(IpAddress), port);
            ServerListener.Bind(ep);
            ServerListener.Listen(100);

            ui.ShowMessage("Server listening...");

            Socket ClientSocket = default(Socket);
            Start start = new Start();
            int counter = 0;
            while (true)
            {
                counter++;
                ClientSocket = ServerListener.Accept();

                ui.ShowMessage(counter + " Clients connected");

                Thread UserThread = new Thread(new ThreadStart(() => start.User(ClientSocket)));
                UserThread.Start();

            }
        }

        public void User(Socket client) 
        {
            int actionId = 0;
            int productId = 0;
            double insertedSum = 0.0;
            byte[] msg = new byte[1024];
            int size=0;

            try
            {
                while (true)
                {
                    size = client.Receive(msg);

                    int.TryParse(System.Text.Encoding.ASCII.GetString(msg, 0, size), out actionId);

                    if(actionId==2)
                    {
                        size = client.Receive(msg);

                        int.TryParse(System.Text.Encoding.ASCII.GetString(msg, 0, size), out productId);
                    } 
                    if(actionId == 3)
                    {
                        size = client.Receive(msg);

                        double.TryParse(System.Text.Encoding.ASCII.GetString(msg, 0, size), out insertedSum);
                    }

                    string results = vendingMachineController.StartVendingMachine(actionId, productId, insertedSum); 

                    byte[] resultBytes = new byte[1384];
                    resultBytes=System.Text.Encoding.ASCII.GetBytes(results);
                    client.Send(resultBytes, 0, resultBytes.Length, SocketFlags.None);
                }
            }
            catch (System.Net.Sockets.SocketException e)
            {
                ui.ShowMessage("A client has just disconnected!");
                
            }
        }
    }
}
