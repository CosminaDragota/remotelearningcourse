﻿using System;
using System.Linq;
using ConsoleAppCodeFirst.DB.Models;

namespace ConsoleAppCodeFirst.BL
{
    public class RefillService
    {
        // Balazs : no need for = null. That is by default null.
        public CategoryService categoryService = null;
        public ProductService productService = null;
        // Balazs : CategoryService, ProductService and existingCategories are not used. Please remove them, and only add back when they are used and you need them.
        public int existingCategories = 0;
        public PaymentService paymentService = null;

        public RefillService()
        {
            categoryService = new CategoryService();
            productService = new ProductService();
            paymentService = new PaymentService();
        }

        public bool RefillQuantities()
        {
            try
            {
                using (var context = new VendingMachineContext())
                {
                    (from p in context.Products
                     select p).ToList().ForEach(x => x.Quantity = 10);

                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public void RefillSum()
        {
            paymentService.InitializeSum(1000);
        }
    }
}
