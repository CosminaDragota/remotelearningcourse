﻿using System;
using System.Linq;
using ConsoleAppCodeFirst.DB.DTO;
using ConsoleAppCodeFirst.DB.Models;
using System.Collections.Generic;

namespace ConsoleAppCodeFirst.BL
{
    public class ReportService
    {
        public List<TransactionDTO> AllSalesReport()
        {
            List<TransactionDTO> transactions, distinctProducts = null;
            try
            {

                using (var context = new VendingMachineContext())
                {
                    transactions = (from t in context.Transactions
                                    where t.BuyProduct == true
                                    select new TransactionDTO
                                    {
                                        TransactionDate = DateTime.Today,
                                        RefillVendingMachine = false,
                                        BuyProduct = true,
                                        ProductId = t.Product.Id,
                                        ProductName = t.Product.Name
                                    }).ToList();
                    distinctProducts = transactions.GroupBy(t => t.ProductId).Select(group => group.First()).ToList();
                }
            }
            catch(Exception e)
            {
                throw;
            }
            return distinctProducts;
        }
        
        public int NumberOfRefillsReport()
        {
            int numberOfRefills = 0;
            try
            {
                using (var context = new VendingMachineContext())
                {
                    numberOfRefills = (from t in context.Transactions
                                       where t.RefillVendingMachine == true
                                       select t).ToList().Count();
                }
            }
            catch(Exception e)
            {
                throw;
            }
            
            return numberOfRefills;
        }
    }
}
