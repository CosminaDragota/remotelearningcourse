﻿using ConsoleAppCodeFirst.DB.Models;
using ConsoleAppCodeFirst.DB.DTO;
using System;

namespace ConsoleAppCodeFirst.BL
{
    public class LogTransactionService
    {
        public void AddRefillTransaction()
        {
            try
            {
                using (var context = new VendingMachineContext())
                {
                    context.Transactions.Add(new Transaction
                    {
                        TransactionDate = DateTime.Now,
                        RefillVendingMachine = true,
                        BuyProduct=false
                    });
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public void AddProductTransaction(ProductDTO productDTO)
        {
            try
            {
                using (var context = new VendingMachineContext())
                {
                    context.Transactions.Add(new Transaction
                    {
                        TransactionDate = DateTime.Now,
                        RefillVendingMachine = false,
                        BuyProduct = true,
                        ProductId = productDTO.Id,
                    });
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
