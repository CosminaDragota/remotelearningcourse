﻿using System;
using System.Linq;
using ConsoleAppCodeFirst.DB.DTO;
using ConsoleAppCodeFirst.DB.Models;

namespace ConsoleAppCodeFirst.BL
{
    public class VendingMachineService
    {
        public RefillService refillService = null;
        public ProductService productService = null;
        public PaymentService paymentService = null;

        public VendingMachineService()
        {
            refillService = new RefillService();
            productService = new ProductService();
            paymentService = new PaymentService();
        }

        public void AddCategories()
        {
            // Balazs : not a good design if you have to use a service from another service. The categoryService should be private in RefillService.
            // If you really need that, you should have categoryService property in this class.
            refillService.categoryService.AddCategories();
        }

        public ProductDTO SelectProduct(int productId)
        {
            ProductDTO productDTO = null;

            try
            {
                productDTO = productService.SelectProduct(productId);
            }
            catch (MyException e)
            {
                throw;
            }
            catch(ArgumentOutOfRangeException e)
            {
                throw;
            }

            return productDTO;
        }

        public (bool, string, double) PayProduct(double insertedSum, double amountToPay)
        { 
            bool isPaid = false;
            double differenceOfMoney = 0.0;
            string message=null;
            try
            {
                if (paymentService.IsAmountValid(insertedSum, amountToPay))
                {
                    differenceOfMoney = paymentService.GiveChange(insertedSum, amountToPay);
                    message=UpdateSum(amountToPay);
                    isPaid = true;
                }
            }
            catch(MyException e)
            {
                throw;
            }
            return (isPaid,message,differenceOfMoney);
        }

        public string UpdateSum(double amountToPay)
        {
            string message = paymentService.AddToSum(amountToPay);
            return message;
        }
    }
}
