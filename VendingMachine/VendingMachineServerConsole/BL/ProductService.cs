﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleAppCodeFirst.DB.Models;
using ConsoleAppCodeFirst.DB.DTO;

namespace ConsoleAppCodeFirst.BL
{
    public class ProductService
    {
        public static IEnumerable<Product> products = null;
        private static readonly Random random = new Random();

        private static double RandomNumberBetween(double minValue, double maxValue)
        {
            var next = random.NextDouble();

            return minValue + (next * (maxValue - minValue));
        }

        public Action<CategoryDTO> AddProducts = (CategoryDTO categoryDTO) =>
          {
              try
              {
                  products = GenerateRandomProducts(categoryDTO);
                  using (var context = new VendingMachineContext())
                  {
                      context.Products.AddRange(products);
                      context.SaveChanges();
                  }

              }
              catch (Exception e)
              {
                  Console.WriteLine("Error addProducts!");
              }
          };

        public static IEnumerable<Product> GenerateChocolateProducts(CategoryDTO categoryDTO)
        {
            IEnumerable<Product> newProducts = new List<Product>();

            try
            {
                newProducts = newProducts.Concat(new[] {
                    new Product { Name = Name.Africana, Price=RandomNumberBetween(3.0,5.0), Quantity=10,CategoryId=categoryDTO.Id}
                    ,new Product { Name = Name.Milka, Price = RandomNumberBetween(3.0, 10.0), Quantity=10,CategoryId = categoryDTO.Id }
                    ,new Product { Name = Name.Poiana, Price = RandomNumberBetween(3.0, 5.0),Quantity=10, CategoryId = categoryDTO.Id }
                    ,new Product { Name = Name.Heidi, Price = RandomNumberBetween(3.0, 9.0),Quantity=10, CategoryId = categoryDTO.Id }
                    ,new Product { Name = Name.Schogetten, Price = RandomNumberBetween(3.0, 4.0),Quantity=10, CategoryId = categoryDTO.Id }
                    ,new Product { Name = Name.Lindt, Price = RandomNumberBetween(3.0, 12.0),Quantity=10, CategoryId = categoryDTO.Id }

                });
                int productsCount = products.Count();
                Console.WriteLine($"Added {productsCount} more chocolates!");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error GenerateRandomChocolate!{e.Message}");
            }

            return newProducts;
        }

        public static IEnumerable<Product> GenerateCoffeeProducts(CategoryDTO categoryDTO)
        {
            IEnumerable<Product> newProducts = new List<Product>();

            try
            {
                newProducts = newProducts.Concat(new[] {
                    new Product { Name = Name.Tchibo, Price=RandomNumberBetween(30.0,50.0),Quantity=10, CategoryId=categoryDTO.Id}
                    ,new Product { Name = Name.Lavazza, Price = RandomNumberBetween(30.0, 100.0), Quantity=10,CategoryId = categoryDTO.Id }
                    ,new Product { Name = Name.Nescafe, Price = RandomNumberBetween(30.0, 6.0), Quantity=10, CategoryId = categoryDTO.Id }
                    ,new Product { Name = Name.Davidoff, Price = RandomNumberBetween(70.0, 100.0), Quantity=10, CategoryId = categoryDTO.Id }

                });
                int productsCount = newProducts.Count();
                Console.WriteLine($"Added {productsCount} more coffees!");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error GenerateRandomCoffee!{e.Message}");
            }

            return newProducts;
        }

        public static IEnumerable<Product> GenerateSodaProducts(CategoryDTO categoryDTO)
        {
            IEnumerable<Product> newProducts = new List<Product>();

            try
            {
                newProducts = newProducts.Concat(new[] {
                    new Product { Name = Name.Pepsi, Price=RandomNumberBetween(5.0,10.0),Quantity=10, CategoryId=categoryDTO.Id}
                    ,new Product { Name = Name.CocaCola, Price = RandomNumberBetween(5.0, 10.0), Quantity=10,CategoryId = categoryDTO.Id }
                    ,new Product { Name = Name.Fanta, Price = RandomNumberBetween(5.0, 15.0), Quantity=10,CategoryId = categoryDTO.Id }
                    ,new Product { Name = Name.Santal, Price = RandomNumberBetween(13.0, 19.0), Quantity=10,CategoryId = categoryDTO.Id }
                    ,new Product { Name = Name.Tymbark, Price = RandomNumberBetween(3.0, 14.0), Quantity=10,CategoryId = categoryDTO.Id }

                });
                int productsCount = newProducts.Count();
                Console.WriteLine($"Added {productsCount} more sodas!");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error GenerateRandomSoda!{e.Message}");
            }

            return newProducts;
        }

        public static IEnumerable<Product> GenerateSticksProducts(CategoryDTO categoryDTO)
        {
            IEnumerable<Product> newProducts = new List<Product>();

            try
            {
                newProducts = newProducts.Concat(new[] {
                    new Product { Name = Name.Snickers, Price=RandomNumberBetween(3.0,5.0), Quantity=10, CategoryId=categoryDTO.Id}
                    ,new Product { Name = Name.Kinder, Price = RandomNumberBetween(3.0, 10.0), Quantity=10, CategoryId = categoryDTO.Id }
                    ,new Product { Name = Name.Lion, Price = RandomNumberBetween(3.0, 5.0),Quantity=10, CategoryId = categoryDTO.Id }
                    ,new Product { Name = Name.Twix, Price = RandomNumberBetween(3.0, 9.0), Quantity=10,CategoryId = categoryDTO.Id }
                    ,new Product { Name = Name.Fitness, Price = RandomNumberBetween(3.0, 4.0), Quantity=10,CategoryId = categoryDTO.Id }

                });

            }
            catch (Exception e)
            {
                Console.WriteLine($"Error GenerateRandomSticks!{e.Message}");

            }
            return newProducts;
        }
        public static IEnumerable<Product> GenerateRandomProducts(CategoryDTO categoryDTO)
        {
            IEnumerable<Product> newProducts = new List<Product>();

            if (categoryDTO.Name == CategoryName.chocolate)
            {
                newProducts= GenerateChocolateProducts(categoryDTO);
            }
            else 
            if (categoryDTO.Name == CategoryName.coffee)
            {
                newProducts= GenerateCoffeeProducts(categoryDTO);
            }
            else 
            if (categoryDTO.Name == CategoryName.soda)
            {
                newProducts= GenerateSodaProducts(categoryDTO);
            }
            else 
            if (categoryDTO.Name == CategoryName.sticks)
            {
                newProducts= GenerateSticksProducts(categoryDTO);
            }
            else
            {
                Console.WriteLine("Couldn't generate products!");
            }
            return newProducts;
        }

        public List<ProductDTO> ListProducts()
        {
            using (var context = new VendingMachineContext())
            {
                List<ProductDTO> productDTOs = GetAll();
                
                return productDTOs;
            }
        }

        public static Func<List<ProductDTO>> GetAll = () =>
        {
            using (var context = new VendingMachineContext())
            {
                IQueryable<ProductDTO> products = from p in context.Products
                                                  select new ProductDTO
                                                  {
                                                      Id = p.Id,
                                                      Name = p.Name,
                                                      Price = p.Price,
                                                      Quantity = p.Quantity,
                                                      CategoryId = p.CategoryId
                                                  };
                return products.ToList();
            }
        };

        public static bool IsIdValid(int productId)
        {
            if (productId < 1)
            {
                throw new MyException("Enter a positive number!");
            }
            using (var context = new VendingMachineContext())
            {
                int firstID = context.Products.First().Id;
                var last = context.Products.OrderByDescending(x => x.Id);
                int lastID = last.First().Id;

                if (productId < firstID || productId > lastID)
                {
                    throw new ArgumentOutOfRangeException($"Enter a number between {firstID} and {lastID}!");                 
                }
                return true;
            }
        }

        public static bool IsInStock(ProductDTO productDTO)
        {
            try
            {
                if (productDTO.Quantity > 0)
                {
                    return true;
                }
                else
                {
                    throw new MyException($"Out of stock for {productDTO.Name}!");
                }
            }
            catch(MyException)
            {
                throw;
            }
            
        }

        public ProductDTO SelectProduct(int productId)
        {
            List<ProductDTO> productDTOs = GetAll();
            try
            {
                if (IsIdValid(productId))
                {
                    ProductDTO product = (from p in productDTOs
                                          where p.Id == productId
                                          select new ProductDTO
                                          {
                                              Id = p.Id,
                                              Name = p.Name,
                                              Price = p.Price,
                                              Quantity = p.Quantity,
                                              CategoryId = p.CategoryId
                                          }).FirstOrDefault();

                    if (IsInStock(product))
                    {
                        return product;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch(MyException e)
            {
                throw;
            }
            catch(ArgumentOutOfRangeException e)
            {
                throw;
            }
            
        }

        public void DecreaseQuantity(ProductDTO productDTO)
        {
            try
            {
                using (var context = new VendingMachineContext())
                {

                    (from p in context.Products
                     where p.Id == productDTO.Id
                     select p).ToList().ForEach(x => x.Quantity = x.Quantity - 1);

                    context.SaveChanges();

                    Product product = (from p in context.Products
                                       where p.Id == productDTO.Id
                                       select p).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
