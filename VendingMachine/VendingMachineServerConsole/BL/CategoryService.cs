﻿using System;
using ConsoleAppCodeFirst.DB.DTO;
using ConsoleAppCodeFirst.DB.Models;

namespace ConsoleAppCodeFirst.BL
{
    public class CategoryService
    {
      
        public Action AddCategories = () =>
        {
            using (var context = new VendingMachineContext())
            {
                context.Categories.Add(new Category { Name = CategoryName.chocolate });
                context.Categories.Add(new Category { Name = CategoryName.coffee });
                context.Categories.Add(new Category { Name = CategoryName.sticks });
                context.Categories.Add(new Category { Name = CategoryName.soda });

                context.SaveChanges();

                foreach (Category category in context.Categories)
                {
                    CategoryDTO dto = new CategoryDTO() { Name = category.Name };
                    Console.WriteLine($"{dto.Name}");

                }
            }
        };          
    }
}
