﻿
namespace ConsoleAppCodeFirst.BL
{
    public class PaymentService
    {
        private double ExistingSum { get; set; }

        public PaymentService()
        {
            InitializeSum(1000);
        }

        public void InitializeSum(double sum)
        {
            ExistingSum = sum;
        }

        public bool IsAmountValid(double insertedSum, double amountToPay)
        {
            double sum = ExistingSum;
            try
            {
                if (insertedSum <= 0 || insertedSum > sum)
                {
                    throw new MyException($"Enter a positive number smaller than {sum}!");
                }
                else if (insertedSum >= amountToPay)
                {
                    return true;
                }
                else
                {
                    throw new MyException("Insert more money!");
                }
            }
            catch (MyException e)
            {
                throw;
            }
        }

        public double GiveChange(double insertedSum, double amountToPay)
        {
            if (insertedSum > amountToPay)
            {
                return insertedSum - amountToPay;
            }
            else
            {
                return 0.0;
            }
        }

        public string AddToSum(double amount)
        {
            ExistingSum = ExistingSum + amount;
            return string.Format($"Sum updated successfully to {ExistingSum}!");
        }
    }
}
