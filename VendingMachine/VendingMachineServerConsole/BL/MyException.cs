﻿using System;

namespace ConsoleAppCodeFirst.BL
{
    public class MyException:SystemException
    {
        private string err_msg;

        public override string Message
        {
            get
            {
                return err_msg;
            }
        }

        public MyException(string errorMessage)
        {
            err_msg = errorMessage;
        }
    }
}
