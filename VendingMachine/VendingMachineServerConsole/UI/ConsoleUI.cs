﻿using System;
using System.Text.RegularExpressions;
using ConsoleAppCodeFirst.DB.DTO;
using System.Collections.Generic;

namespace ConsoleAppCodeFirst.UI
{
    public class ConsoleUI
    {
        private const string AnswerRegex = @"^[a-mo-xz-zA-MO-XZ-Z0-9*#+=-]+$";
        private const string MoneyRegex = @"^[a-z]*$";

        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
        public void ShowPrice(ProductDTO productDTO)
        {
            double amount = productDTO.Price;
            Console.WriteLine($"The amount to pay is: {productDTO.Price}");
        }

        public void ShowProductDetails(ProductDTO productDTO)
        {
            Console.WriteLine($"You selected: \n {productDTO.ToString()}");
        }

        public void ShowErrorMessage(string message)
        {
            Console.WriteLine(message);
        }

        public void ShowPaymentStatus(ProductDTO productDTO, bool isPaid, string message, double change)
        {
            if (isPaid)
            {
                Console.WriteLine($"Product {productDTO.Name} paid successfully!");
                Console.WriteLine($"{message}");
                Console.WriteLine($" Your change is {change}!");
            }
            else
            {
                Console.WriteLine($"Product {productDTO.Name} not paid!");
            }
        }

        public void ShowRefillStatus(bool success)
        {
            if (success)
            {
                Console.WriteLine($"Refilled successfully!");
            }
            else
            {
                Console.WriteLine($"Refill failed!");
            }
        }

        public void ShowAllSalesReport(List<TransactionDTO> allSales)
        {
            foreach (TransactionDTO transaction in allSales)
            {
                Console.WriteLine(transaction.ToString());
            }
        }

        public void ShowRefillsReport(int refills)
        {
            Console.WriteLine($"The vending machine was refilled {refills} times!");
        }

        public double PaymentInput()
        {
            double insertedSum = 0.0;
            bool changeAmount = false;
            do
            {
                try
                {
                    changeAmount = false;
                    Console.Write("\n Enter amount: ");
                    double.TryParse(Console.ReadLine().ToString(), out insertedSum);

                    if (Regex.IsMatch(insertedSum.ToString(), MoneyRegex))
                    {
                        changeAmount = true;
                        throw new FormatException();
                    }
                    else
                    {
                        changeAmount = false;
                    }
                }
                catch (FormatException e)
                {
                    Console.WriteLine("Enter a number!");
                }
            } while (changeAmount);

            return insertedSum;
        }

        public void ShowProducts(List<ProductDTO> list)
        {
            foreach (ProductDTO p in list)
            {
                Console.WriteLine(p.ToString());
            }
        }

        public int ProductInput()
        {
            int productId = 0;
            bool selectProduct = false;
            do
            {
                Console.WriteLine("Insert product ID:");

                try
                {
                    int.TryParse(Console.ReadLine().ToString(), out productId);
                    if (productId == 0)
                    {
                        Console.WriteLine("Input not valid! Please enter a number!");
                        selectProduct = true;
                    }
                    else
                    {
                        selectProduct = false;
                    }
                }
                catch (System.NullReferenceException ex)
                {
                    Console.WriteLine($"{ex.Message}! Input not valid!");
                    selectProduct = true;
                }

            } while (selectProduct);

            return productId;
        }

        public int CategoryInput()
        {
            bool selectCategory = false;
            int categoryId = 0;

            do
            {
                Console.WriteLine("Select category by ID:");

                try
                {
                    int.TryParse(Console.ReadLine().ToString(), out categoryId);
                    Console.WriteLine($"id: {categoryId}");

                    if (categoryId == 0)
                    {
                        Console.WriteLine("Input not valid! Please enter a number!");
                        selectCategory = true;
                    }
                    else
                    {
                        selectCategory = false;
                    }
                }
                catch (System.NullReferenceException ex)
                {
                    Console.WriteLine($"{ex.Message}! Input not valid!");
                    selectCategory = true;
                }
            } while (selectCategory);

            return categoryId;
        }

        public void ShowDefaultOption(string menuType)
        {
            if (menuType == "ADMIN")
            {
                Console.WriteLine("Select one of the following options: 1,4,5,6!");

            }
            if (menuType == "CLIENT")
            {
                Console.WriteLine("Select one of the following options: 1,2,3!");
            }
            if (menuType == null)
            {
                Console.WriteLine("Select an option between 1-6!");
            }
        }

        public void ShowGoodbyeMessage()
        {
            Console.WriteLine("Have a good day!");
            Console.ReadKey();
        }

        public int DisplayMenu(string menuType)
        {
            int actionId = 0;
            if (menuType == "ADMIN")
            {
                Console.WriteLine("Options:\n");
                Console.WriteLine("1.List products");
                Console.WriteLine("4.Refill quantities and money");
                Console.WriteLine("5.All Sales Report");
                Console.WriteLine("6.Number of Refills Report");
            }
            if (menuType == "CLIENT")
            {
                Console.WriteLine("Options:\n");
                Console.WriteLine("1.List products");
                Console.WriteLine("2.Select product");
                Console.WriteLine("3.Pay Product");
            }
           
            Console.WriteLine("\nEnter option:");
            int.TryParse(Console.ReadLine().ToString(), out actionId);

            return actionId;
        }      

        public bool CanDisplayMenu()
        {
            bool repeat = false;
            do
            {
                try
                {
                    Console.WriteLine("\nDo you want to select another option? [y/n]");
                    string choose = Console.ReadLine().ToString();

                    if (Regex.IsMatch(choose, AnswerRegex))
                    {
                        throw new FormatException();
                    }
                    else
                    {
                        if (choose == "y")
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Choose a letter: y/n");
                    repeat = true;
                }
            } while (repeat);
            return true;
        }
    }
}
