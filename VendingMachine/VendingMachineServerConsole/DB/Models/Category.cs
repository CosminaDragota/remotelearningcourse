﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConsoleAppCodeFirst.DB.Models
{
    public enum CategoryName
    {
        chocolate, sticks, soda, coffee
    }

    public class Category
    {
        [Key]
        public int Id { get; set; }

        [Column("CategoryName")]
        public CategoryName Name { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public Category() { }

        public override string ToString()
        {
            return string.Format($"ID: {Id} \n Name: {Name}");
        }
    }
}
