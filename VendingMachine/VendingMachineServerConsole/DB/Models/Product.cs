﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConsoleAppCodeFirst.DB.Models
{
    public enum Name
    {
        Milka, Poiana, Heidi, Africana, Schogetten, Lindt,
        Snickers, Kinder, Lion, Twix, Fitness,
        Pepsi, CocaCola, Tymbark, Fanta, Santal,
        Tchibo, Lavazza, Nescafe, Davidoff
    }

    public class Product
    {   
        [Key]
        public int Id { get; set; }

        [Column("ProductName")]
        public Name Name { get; set; }

        [Column("ProductPrice")]
        public double Price { get; set; }

        [Column("Quantity")]
        public int Quantity { get; set; }

        [Column("CategoryID")]
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public Product() { }
               
        public override string ToString()
        {
            return string.Format($"Name: {Name};\n Price: {Price}; \n Quantity:{Quantity}\n ");
        }
    }
}

