﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace ConsoleAppCodeFirst.DB.Models
{
    public class Transaction
    {
        [Key]
        public int Id { get; set; }

        [Column("TransactionDate")]
        public DateTime TransactionDate { get; set; }

        [Column("RefillVendingMachine")]
        public bool RefillVendingMachine { get; set; }

        [Column("BuyProduct")]
        public bool BuyProduct { get; set; }

        [Column("ProductID")]
        public int? ProductId { get; set; }
        public virtual Product Product { get; set; }

        public Transaction() { }

        public override string ToString()
        {
            return string.Format($"\nProduct: {Product.Name} \n Purchased on: {TransactionDate}\n");
        }
    }
}
