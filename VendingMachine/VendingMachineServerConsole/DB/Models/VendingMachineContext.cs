﻿using System.Data.Entity;

namespace ConsoleAppCodeFirst.DB.Models
{
    public class VendingMachineContext: DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        public VendingMachineContext() : base("name=DataModel")
        {
            Configuration.AutoDetectChangesEnabled = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Category>().HasMany(x => x.Products).WithRequired(x => x.Category).HasForeignKey(x => x.CategoryId);
            modelBuilder.Entity<Transaction>().HasOptional(x => x.Product);
        }
    }
}
