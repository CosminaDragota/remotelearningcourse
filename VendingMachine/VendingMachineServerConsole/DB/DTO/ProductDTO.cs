﻿using ConsoleAppCodeFirst.DB.Models;

namespace ConsoleAppCodeFirst.DB.DTO
{
    public class ProductDTO
    {
        public int Id { get; set; }

        public Name Name { get; set; }

        public double Price { get; set; }

        public int Quantity { get; set; }

        public int CategoryId { get; set; }

        public ProductDTO() { }

        public override string ToString()
        {
            return string.Format($"ID: {Id}\n Name: {Name};\n Price: {Price} RON; \n Quantity:{Quantity}\n ");
        }
    }
}
