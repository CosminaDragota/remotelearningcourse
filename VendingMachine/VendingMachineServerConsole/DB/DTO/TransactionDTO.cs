﻿using System;
using ConsoleAppCodeFirst.DB.Models;

namespace ConsoleAppCodeFirst.DB.DTO
{
    public class TransactionDTO
    {
        public int Id { get; set; }

        public DateTime TransactionDate { get; set; }

        public bool RefillVendingMachine { get; set; }

        public bool BuyProduct { get; set; }

        public int? ProductId { get; set; }

        public Name ProductName { get; set; }

        public TransactionDTO() { }

        public override string ToString()
        {
            return string.Format($"\nProduct: {ProductName} \n Purchased on: {TransactionDate}\n");
        }
    }
}
