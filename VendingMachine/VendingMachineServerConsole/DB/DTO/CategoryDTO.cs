﻿using ConsoleAppCodeFirst.DB.Models;

namespace ConsoleAppCodeFirst.DB.DTO
{
    public class CategoryDTO
    {
        public int Id { get; set; }

        public CategoryName Name { get; set; }

        public CategoryDTO() { }

        public override string ToString()
        {
            return string.Format($"{Id}. {Name}");
        }
    }
}
