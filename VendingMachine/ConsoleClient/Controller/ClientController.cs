﻿using System.Net;
using System.Net.Sockets;
using ConsoleAppCodeFirst.UI;

namespace ConsoleClient
{
    // Balazs : this class has dual responsibility. One for socket communication, and the other for controller.
    public class ClientController
    {
        private static ConsoleUI ui = new ConsoleUI();
        private static int port = 8001;
        private static string IpAddress = "127.0.0.1";
        private static Socket ClientSocket = new Socket(AddressFamily
            .InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static IPEndPoint ep = new IPEndPoint(IPAddress.Parse(IpAddress), port);
        private static string menuType = "CLIENT";

        static void Main(string[] args)
        {
            bool repeat = true;
            ClientSocket.Connect(ep);
            ui.ShowMessage("Client is connected!");

            while (repeat)
            {
                repeat=StartClientFlow();
                ui.ShowGoodbyeMessage();
            }
        }

        public static bool StartClientFlow()
        {
            int actionId = 0;
            int productId = 0;
            double insertedSum = 0.0;
            bool repeat= false;
            do
            {
                actionId = ui.DisplayMenu(menuType);

                if (actionId == 1)
                {
                    ClientSocket.Send(System.Text.Encoding.ASCII.GetBytes(actionId.ToString()), 0, actionId.ToString().Length, SocketFlags.None);
                    ReceiveMesssageFromServer();
                }
                if (actionId == 2)
                {
                    productId = ui.ProductInput();
                    ClientSocket.Send(System.Text.Encoding.ASCII.GetBytes(actionId.ToString()), 0, actionId.ToString().Length, SocketFlags.None);
                    ClientSocket.Send(System.Text.Encoding.ASCII.GetBytes(productId.ToString()), 0, productId.ToString().Length, SocketFlags.None);

                    ReceiveMesssageFromServer();
                }
                if (actionId == 3)
                {
                    if (productId != 0) //daca s-a ales un produs
                    {
                        insertedSum = ui.PaymentInput();

                    }
                    ClientSocket.Send(System.Text.Encoding.ASCII.GetBytes(actionId.ToString()), 0, actionId.ToString().Length, SocketFlags.None);
                    ClientSocket.Send(System.Text.Encoding.ASCII.GetBytes(insertedSum.ToString()), 0, insertedSum.ToString().Length, SocketFlags.None);

                    ReceiveMesssageFromServer();              
                }
                if(actionId!=1 && actionId!=2 && actionId!=3)
                {
                    ui.ShowDefaultOption(menuType);
                }
                repeat = ui.CanDisplayMenu();
            } while (repeat);
            return repeat;
        }

        public static void ReceiveMesssageFromServer()
        {
            byte[] messageFromServer = new byte[5000];
            int size = ClientSocket.Receive(messageFromServer);

            string response = System.Text.Encoding.ASCII.GetString(messageFromServer, 0, size);
            ui.ShowMessage("Server response:\n" + response);
        }
    }
}
